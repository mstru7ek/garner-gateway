#!/bin/bash


echo "----------------------------------------------------------- |"
echo "Build certs"
echo  ""

echo "----------------------------------------------------------- | del old"
echo "Removed keystore.p12"
rm -rf  keystore.p12


echo "----------------------------------------------------------- | build"
openssl req -x509 -nodes -days 730 -newkey rsa:2048 \
        -keyout server.key -out server.crt -config req.conf \
        -extensions 'v3_req'

echo "----------------------------------------------------------- | CACertFile"
cp server.crt CACertifcate.crt

echo "----------------------------------------------------------- | create keystore"
openssl pkcs12 -inkey server.key -in server.crt -export \
        -name garner -out keystore.p12 \
        -passin pass:'' -passout pass:'garner'
        

echo "----------------------------------------------------------- | verify modulus"
openssl x509 -noout -modulus -in server.crt > server.crt_mod
openssl rsa -noout -modulus -in server.key > server.key_mod

echo "        >>>>        MODULUS DIFF        <<<<                |"
echo ""

diff server.crt_mod server.key_mod

echo "----------------------------------------------------------- |"
echo  ""


opensssl
=============================


Nowy self-signed certyfikat z config pliku


Utowrzy plik konfiguracji dla certyfikatu
-----------------------------

req.conf
```
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no
[req_distinguished_name]
C = PL
ST = MZ
L = Warsaw
O = mstru7ek
OU = mstru7ek
CN = server500003.nazwa.pl

[v3_req]
subjectKeyIdentifier    = hash
subjectAltName          = @alt_names

# install as user root cert on Android
basicConstraints   		= critical,CA:true	 

[alt_names]
DNS.1 = server500003.nazwa.pl
DNS.2 = garner-gateway
DNS.3 = garner-autho
DNS.4 = garner-service
DNS.5 = garner-dbx


```


Poniewaz "Ribbon" rozwiazuje certyfikate po nazwie jak rowniez po SubjectAlternativeNames

Wygeneruj prywatny klucz i certyfikat
-----------------------------

  openssl req -x509 -nodes -days 730 -newkey rsa:2048 -keyout server.key -out server.crt -config req.conf -extensions 'v3_req'

Spakuj klucz praywatny wraz z certyfikatem do KeyStore:
-----------------------------

  openssl pkcs12 -inkey key.pem -in certificate.pem -export -out certificate.p12


Konwersja na JKS ( OBOWIAZKOWO !!! )
-----------------------------
keytool -importkeystore -destkeystore certificate.p12 \
        -srckeystore keystore.p12 -srcstoretype pkcs12
        -alias garner -storepass garner

Weryfikacja
-----------------------------

    openssl x509 -text -noout -in certificate.pem

    openssl pkcs12 -in certificate.p12 -noout -info


KeyStore type : PKCS12
Provider : SUN

Resources
------------------------------

https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html



keytool
=============================

Default format for TrustStore & KeyStore is : Java KeyStore - JKS [ SUN ]


`cacerts` defautl PASSWORD : 'changeit'


keytool -importcert -file garner.pem -keystore cacerts -storepass changeit -noprompt



Self-singed keystore with "PrivateKeyEntry"
-----------------------------

keytool -genkeypair -v \
  -alias garner  \
  -dname "CN=server500003.nazwa.pl, OU=mstru7ek, O=mstru7ek, L=Warsaw, ST=Warsaw, C=PL"  \
  -keystore keystore.p12  \
  -storetype PKCS12 \
  -storepass garner \
  -keyalg RSA  \
  -keysize 2048  \
  -ext SubjectAlternativeName:critical="DNS:server500003.nazwa.pl,DNS:garner-gateway,DNS:garner-service"  \
  -validity 9999 


  !!!! Web Browser support self-signed Chrome, Opera only if
  !!!! common name in [ "-ext SubjectAlternativeName:critical="DNS:server500003.nazwa.pl" ]


List KeyStore & Details
-----------------------------

    keytool -list -v -keystore keystore.p12 -storepass garner

List type and all certificates with aliases form KeyStore, TrustStore.


Export Certificate from KeyStore
-----------------------------

	keytool -exportcert -alias garner -file garner.pem -rfc -keystore keystore.p12 -storepass garner

Export 'garner' certificate from key store into RFC pem format file.



Export Private Key 
----------------------------

    openssl pkcs12 -in keystore.p12  -nodes -nocerts -out server.pem   

Create TrustStore for java TLS
-----------------------------

	keytool -importcert -file garner.pem -alias garner -keystore cacerts -storepass garner -noprompt

Append or Create

Import certificate from file as alias garner into keystore cacerts ( create if not exist ) with store password, 
confirm all actions without promt.



Get PEM Certificate from Socket
-----------------------------
	
	keytool -printcert -rfc -sslServer onet.pl > onet.pem

View certificate in  PEM - Privacy-Enhanced Mail , format (RFC 7468).


Weryfikacja
------------------------------

    keytool -list -v -keystore keystore.p12  -storepass garner

Weryfikacja.


Weryfikacja Certyfikatu ( Public Key ) i Private Key
------------------------------

    echo "--Certificate:"  && \
    openssl x509 -noout -modulus -in server.crt && \
    echo "--Key:"  && \
    openssl rsa -noout -modulus -in server.key

Moduly powinny byc identyczne.    



Linki: https://www.ibm.com/support/knowledgecenter/en/SSHS8R_7.1.0/com.ibm.worklight.installconfig.doc/admin/t_installing_root_CA_android.html
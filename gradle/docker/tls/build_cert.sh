#!/bin/bash


# SPRAWDZIC

echo "----------------------------------------------------------- |"
echo "Build certs"
echo  ""

echo "----------------------------------------------------------- | del old"
echo "Removed keystore.p12"
rm -rf  keystore.p12

echo "----------------------------------------------------------- | build"
keytool -genkeypair -v \
  -alias garner  \
  -dname "CN=server500003.nazwa.pl, OU=mstru7ek, O=mstru7ek, L=Warsaw, ST=Warsaw, C=PL"  \
  -keystore keystore.p12  \
  -storetype PKCS12 \
  -storepass garner \
  -keyalg RSA  \
  -keysize 2048  \
  -ext SubjectAlternativeName:critical="DNS:server500003.nazwa.pl,DNS:garner-gateway,DNS:garner-service"  \
  -validity 9999

echo "----------------------------------------------------------- | list"
keytool -keystore keystore.p12 -storepass garner -list

echo "----------------------------------------------------------- | server.pem"
keytool -exportcert -alias garner -file server.crt -rfc -keystore keystore.p12 -storepass garner
cp server.crt CACertifcate.crt

echo "----------------------------------------------------------- | server.key"
openssl pkcs12 -in keystore.p12 -passin pass:garner -nodes -nocerts -out server.key   


echo "----------------------------------------------------------- | verify modulus"
openssl x509 -noout -modulus -in server.crt > server.crt_mod
openssl rsa -noout -modulus -in server.key > server.key_mod

echo "        >>>>        MODULUS DIFF        <<<<                |"
diff server.crt_mod server.key_mod

echo "----------------------------------------------------------- |"
echo  ""

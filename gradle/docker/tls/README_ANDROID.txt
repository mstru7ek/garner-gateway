
Instalacja Certyfikatu Na Android
=============================


Przygotuj
-----------------------------

Pobierz hash pod nazwe pliku.

	openssl x509 -inform PEM -subject_hash -in server.crt | head -1	

Z dowolnej formy przekonwertuj na werjse textowa.

    openssl x509 -inform PEM -in 388d1e04.pem -text > 0437a769.0

lub z innej postaci:

    openssl x509 -inform DER -in 388d1e04.crt -text > 388d1e04.0


Zamien miejscami ( naglowek PEM a pozniej opis certyfikatu)

Format wyjsiowy:

```
PEM
+ 
OPIS

```

Zapisz
-----------------------------

Podlacz telefon.

    WAZNE !!! : USUNAC POPRZEDNI CERTYFIKAT DLA TEJ SAMEJ DOMENY.

Przekopiuj plik na telefon.
	
	cp  388d1e04.0 [PHONE/] /sdcard/388d1e04.0
	
Wejdz w ustawienia

		"Ekrany blokady i zabezpiecznia" > "Innes Ustawienia zabezpieczen" > "Instalacja z pamieci urzadzenia".
		
Zweryfikuj poprawnie zainstalowany certyfikat.

    "Ekrany blokady i zabezpiecznia" > "Innes Ustawienia zabezpieczen" > "Certyfikaty uzytkownika".

    "Ekrany blokady i zabezpiecznia" > "Innes Ustawienia zabezpieczen" > "Pokaz certyfikaty zabezpieczen" > "UZYTKOWNIK".


Link:
https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html





    
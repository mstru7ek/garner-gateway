package pl.garner.gateway;

import javax.net.ssl.*;
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.util.Collection;
import java.util.List;

public class SSLConnection {


    private static final String host = "server500003.nazwa.pl";
    private static final int port =  433;

//    private static final char[] PASSPHRASE = "garner".toCharArray();

    private static final char[] PASSPHRASE = null;


    /**
     * Utility class.
     *
     * All functionality could be resolved instead with keytool & bash automation.
     *
     * What it does ?
     *
     * 1. Load Java TrustStore from current folder or JAVA_HOME.
     * 2. Creates SavingTrustManager to "spy" on certificate chain during SSL connection.
     * 3. Establish connection over SSL with custom host.
     *
     * If host is properly set in local TrustStore connection should work without any complications.
     *
     */
    public static void main(String[] args) {

        KeyStore ks = loadKeyStore();
        if (ks == null) {
            System.out.println("null key store");
            return;
        }

        TrustManagerFactory tmf = null;
        try {
            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        }

        try {
            tmf.init(ks);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            return;
        }

        X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];


        /**
         * Save certificates upon connection request.
         *
         */
        SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);

        SSLContext context = null;
        try {

            context = SSLContext.getInstance("TLS");

            context.init(null, new TrustManager[]{tm}, null);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        if (context== null) {
            System.exit(127);
        }

        /**
         * Create TLS socket
         */
        SSLSocketFactory factory = context.getSocketFactory();


        System.out.println("Opening connection to " + host + ":" + port + "...");
        SSLSocket socket = null;
        try {
               socket = (SSLSocket) factory.createSocket(host, port);
            socket.setSoTimeout(10000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if( socket == null) {
            System.exit(127);
        }

        try {
            System.out.println("Starting SSL handshake...");
            socket.startHandshake();
            socket.close();
            System.out.println();
            System.out.println("No errors, certificate is already trusted");
        } catch (SSLException e) {
            System.out.println();
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SSLSession session = socket.getSession();


        if (session !=null) {

            try {
                Certificate[] certs = session.getPeerCertificates();
                X509Certificate x509 = (X509Certificate)certs[0];
                Collection<List<?>> subjectAlternativeNames = x509.getSubjectAlternativeNames();

                for (List<?> san : subjectAlternativeNames) {
                    System.out.println("SAN : " + san);
                }


            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (CertificateParsingException e) {
                e.printStackTrace();
            }


        }

        X509Certificate[] chain = tm.chain;
        if (chain == null) {
            System.out.println("Could not obtain server certificate chain");
            return;
        }

        System.out.println();
        System.out.println("Server sent " + chain.length + " certificate(s):");
        System.out.println();

        MessageDigest sha1 = getMessageDigest("SHA1");
        MessageDigest md5 = getMessageDigest("MD5");

        for (int i = 0; i < chain.length; i++) {
            X509Certificate cert = chain[i];

            byte[] sha1Digest = getDigest(sha1, cert);
            byte[] md5Digest = getDigest(sha1, cert);

            getDigest(md5, cert);

            System.out.println(" " + (i + 1) + " Subject " + cert.getSubjectDN());
            System.out.println("   Issuer  " + cert.getIssuerDN());
            System.out.println("   sha1    " + toHexString(sha1Digest));
            System.out.println("   md5     " + toHexString(md5Digest));
            System.out.println();
        }


/*
        try {
            appendCertificate(ks, chain);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

    }

    private static byte[] getDigest(MessageDigest messageDigest, X509Certificate cert) {
        try {
            messageDigest.update(cert.getEncoded());
            return messageDigest.digest();
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static MessageDigest getMessageDigest(String type) {
        try {
            return MessageDigest.getInstance(type);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * jssecacerts
     * @return
     */
    private static KeyStore loadKeyStore() {

        /**
         * Find CA trust store
         */
        File file = new File("jssecacerts");
        if (file.isFile() == false) {
            char SEP = File.separatorChar;
            File dir = new File(System.getProperty("java.home") + SEP
                + "lib" + SEP + "security");
            file = new File(dir, "jssecacerts");
            if (file.isFile() == false) {
                file = new File(dir, "cacerts");
            }
        }

        /**
         * Load CA into KeyStore
         */
        System.out.println("Loading KeyStore " + file + "...");
        InputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(in, PASSPHRASE);
            return ks;
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static void appendCertificate(KeyStore ks, X509Certificate[] chain) throws Exception {

        BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));


        System.out.println("Enter certificate to add to trusted keystore or 'q' to quit: [1]");
        String line = reader.readLine().trim();
        int k;
        try {
            k = (line.length() == 0) ? 0 : Integer.parseInt(line) - 1;
        } catch (NumberFormatException e) {
            System.out.println("KeyStore not changed");
            return;
        }

        X509Certificate cert = chain[k];
        String alias = host + "-" + (k + 1);
        ks.setCertificateEntry(alias, cert);

        OutputStream out = new FileOutputStream("jssecacerts");
        ks.store(out, PASSPHRASE);
        out.close();

        System.out.println();
        System.out.println(cert);
        System.out.println();
        System.out.println("Added certificate to keystore 'jssecacerts' using alias '"                + alias + "'");
    }

    private static class SavingTrustManager implements X509TrustManager {

        private final X509TrustManager tm;
        private X509Certificate[] chain;

        SavingTrustManager(X509TrustManager tm) {
            this.tm = tm;
        }

        public X509Certificate[] getAcceptedIssuers() {

            /**
             * This change has been done due to the following resolution advised for Java 1.7+
             http://infposs.blogspot.kr/2013/06/installcert-and-java-7.html
             **/

            return new X509Certificate[0];
            //throw new UnsupportedOperationException();
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
            throw new UnsupportedOperationException();
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
            this.chain = chain;
            tm.checkServerTrusted(chain, authType);
        }
    }

    private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 3);
        for (int b : bytes) {
            b &= 0xff;
            sb.append(HEXDIGITS[b >> 4]);
            sb.append(HEXDIGITS[b & 15]);
            sb.append(' ');
        }
        return sb.toString();
    }

}

package pl.garner.gateway;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import pl.garner.gateway.config.ApplicationProperties;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class RedirectResources {

    private static final Logger log = LoggerFactory.getLogger(RedirectResources.class);

    private static final String ENC_UTF_8 = "UTF-8";
    private static final String ENV_HOST_URL = "HOST_URL";

    private final Environment env;
    private final ApplicationProperties applicationProperties;

    @RequestMapping(value = "/dbx/preferences/access-granted")
    public RedirectView dropboxAccessGranted(HttpServletRequest httpServletRequest) throws UnsupportedEncodingException {

        Map<String, String> queryMap = QueryUtils.splitQuery(httpServletRequest.getQueryString());

        String state = getEncodeQueryParam(queryMap, "state");
        String code = getEncodeQueryParam(queryMap, "code");
        String error = getEncodeQueryParam(queryMap, "error");

        StringBuilder queryParams = new StringBuilder();
        if (error != null && !error.isEmpty()) {
            queryParams.append("error/").append(error);
        } else {
            queryParams.append(state).append("/").append(code);
        }

        RedirectView redirectView = new RedirectView(getAccessGrantedUri() + "/" + queryParams.toString(), false);
        return redirectView;
    }

    private String getEncodeQueryParam(Map<String, String> queryMap, String queryParamName) throws UnsupportedEncodingException {
        String paramValue = queryMap.get(queryParamName);
        if (paramValue != null) {
            return URLEncoder.encode(paramValue, ENC_UTF_8);
        }
        return null;
    }

    private String getAccessGrantedUri(){
        String accessGrantedPath  = applicationProperties.getAccessGrantedUri();
        String accessGrantedUri = env.getProperty(ENV_HOST_URL) + accessGrantedPath;

        log.info("AccessGrantedUri = " + accessGrantedUri);

        return accessGrantedUri;
    }

}

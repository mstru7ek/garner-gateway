import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'

import { AppModule } from './app/grn-app.module'
import { environment } from './environments/environment'
import { hmrBootstrap } from './hmr'

if (environment.production) {
  enableProdMode()
}

const bootstrap = () => {
  const handler = platformBrowserDynamic().bootstrapModule(AppModule)
  handler
    .then(_ => console.log(`Bootstrap success`))
    .catch(err => console.error(err))
  return handler
}

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap)
  } else {
    console.error('HMR is not enabled for webpack-dev-server!')
    console.log('Are you using the --hmr flag for ng serve?')
  }
} else {
  bootstrap()
}

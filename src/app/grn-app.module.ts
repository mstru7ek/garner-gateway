import { HttpClientModule } from '@angular/common/http'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { PageModule } from 'app/page/page.module'
import { SharedModule } from 'app/shared/shared.module'

import { WordModule } from 'app/word/word.module'
import { ServiceModule } from 'app/service/service.module'
import { MapModule } from 'app/map/map.module'
import { PreferenceModule } from 'app/preference/preference.module'
import { NamespaceModule } from 'app/namespace/namespace.module'
import { GraphModule } from 'app/graph/graph.module'

import { InterceptorsModule } from 'app/interceptors'
import { LayoutModule } from 'app/layouts/layout.module'
import { MainComponent } from 'app/layouts'

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    InterceptorsModule,
    NgbModule,
    SharedModule,
    LayoutModule,
    PageModule,
    NamespaceModule,
    WordModule,
    MapModule,
    GraphModule,
    PreferenceModule,
    ServiceModule
  ],
  bootstrap: [MainComponent],
  declarations: []
})
export class AppModule { }

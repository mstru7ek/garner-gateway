import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DropboxService, EventManager } from 'app/service';
import { Logger, LoggerService } from 'app/shared';

@Component({
  selector: 'grn-dbx-access-granted',
  templateUrl: './dbx-access-granted.component.html',
  styleUrls: ['./dbx-access-granted.component.scss']
})
export class DbxAccessGrantedComponent implements OnInit {

  private logger: Logger

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dropboxService: DropboxService,
    private eventManager: EventManager,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('DbxAccessGrantedComponent')
  }

  ngOnInit() {
    this.eventManager.subscribe('dropbox.access-token.change', _ => {
      this.dropboxService.getAccessToken().subscribe( token => {
        if (token.valid) {
          this.router.navigate(['/preferences'])
        }
      })
    })
    this.activatedRoute.paramMap.subscribe(queryParam => {
      if (queryParam.has('error')) {
        const dbxError = queryParam.get('error')
        this.logger.error(' Dbx connection error with code: ', dbxError)
        return
      }
      if (queryParam.get('state') && queryParam.get('code')) {
        const dbxState = queryParam.get('state')
        const dbxCode = queryParam.get('code')
        this.dropboxService.setAccessAgranted(dbxState, dbxCode)
      }
    })
  }
}

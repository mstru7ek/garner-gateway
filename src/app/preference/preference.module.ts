import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'
import { SyncViewComponent, DbxAccessGrantedComponent, PreferenceViewComponent } from './'
import { ServiceModule } from 'app/service/service.module'
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    ServiceModule
  ],
  declarations: [
    PreferenceViewComponent,
    SyncViewComponent,
    DbxAccessGrantedComponent
  ],
  exports: [
    PreferenceViewComponent,
    SyncViewComponent,
    DbxAccessGrantedComponent
  ]
})
export class PreferenceModule { }

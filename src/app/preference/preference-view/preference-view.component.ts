import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountInfo, DbxAccessToken, NamespaceEntity, PreferenceEntity } from 'app/model';
import { DropboxService, EventManager, NamespaceService, PreferenceService } from 'app/service';
import { FormUtils, Logger, LoggerService, PreloadService } from 'app/shared';
import { Principal } from 'app/shared/auth';
import { Observable } from 'rxjs';

@Component({
  selector: 'grn-preference-view',
  templateUrl: './preference-view.component.html',
  styleUrls: ['./preference-view.component.scss']
})
export class PreferenceViewComponent implements OnInit {

  private logger: Logger

  accountInfo: Promise<AccountInfo> | null = null
  namespaces: Observable<NamespaceEntity[]> | null = null
  userPreference: Observable<PreferenceEntity> | null = null
  isEditMode = false

  dbxAccessToken: Observable<DbxAccessToken> | null = null;

  accountInfoForm: FormGroup

  formErrors: { [key: string]: string } = {
    'namespace': ''
  }

  validationMessages = {
    'namespace': {
      'required': 'Namespace is required.',
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private principal: Principal,
    private preferencesService: PreferenceService,
    private namespaceService: NamespaceService,
    private dropboxService: DropboxService,
    private loggerService: LoggerService,
    private eventManager: EventManager,
    private preloadService: PreloadService,
  ) {
    this.logger = this.loggerService.getInstance('PreferenceViewComponent')
  }

  ngOnInit() {
    this.accountInfo = this.principal.identity()
    this.namespaces = this.namespaceService.list()
    this.userPreference = this.preferencesService.getUserPreferences()

    this.dbxAccessToken = this.dropboxService.getAccessToken()
    this.eventManager.subscribe('dropbox.access-token.change', _ => {
      this.dbxAccessToken = this.dropboxService.getAccessToken()
    })
  }

  buildForm() {
    this.userPreference.subscribe(userPreference => {
      this.accountInfoForm = this.formBuilder.group({
        'namespace': [userPreference.namespace, [Validators.required]],
      })
      this.accountInfoForm.valueChanges.subscribe(_ => this.onValueChanged())
      this.onValueChanged() // (re)set validation messages now
    })
  }

  onValueChanged() {
    this.formErrors = FormUtils.buildValidationErrors(this.accountInfoForm, this.formErrors, this.validationMessages)
  }

  onEdit($event) {
    this.isEditMode = true
    this.buildForm()
  }

  onSubmit() {
    this.preloadService.show()
    this.isEditMode = false
    this.preferencesService.setDefaultNamspace(this.accountInfoForm.value.namespace).subscribe(preference => {
      const namespace: NamespaceEntity = this.accountInfoForm.value.namespace
      this.logger.info(' + set default namespace done, namespace.id', namespace.id)
      this.logger.info(' + set default namespace done, namespace.name', namespace.name)
      this.userPreference = this.preferencesService.getUserPreferences()
      this.userPreference.subscribe(_ => this.preloadService.hide())
    })
  }

  onConnect() {
    this.dropboxService.connect()
  }

  onDisconnect() {
    this.dropboxService.disconnect()
  }

  onCancel() {
    this.isEditMode = false
    this.accountInfoForm.reset()
  }

  equalNamespaces: (ns1: NamespaceEntity, ns2: NamespaceEntity) => boolean = (ns1, ns2) => {
    return ns1 && ns2 ? ns1.id === ns2.id : ns1 === ns2
  }
}

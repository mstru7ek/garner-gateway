import { Component, OnInit } from '@angular/core';
import { DbxAccessToken } from 'app/model';
import { DropboxService } from 'app/service';
import { Logger, LoggerService } from 'app/shared';
import { Observable } from 'rxjs';

@Component({
  selector: 'grn-sync-view',
  templateUrl: './sync-view.component.html',
  styleUrls: ['./sync-view.component.scss']
})
export class SyncViewComponent implements OnInit {

  private logger: Logger

  dbxAccessToken: Observable<DbxAccessToken> | null = null;
  response: Observable<string> | null = null;

  inprogress = false

  result: string = null

  constructor(
    private dropboxService: DropboxService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('SyncViewComponent')
  }

  ngOnInit() {
    this.dbxAccessToken = this.dropboxService.getAccessToken()
  }

  onStart() {
    this.inprogress = true

    this.dropboxService.synchronize().subscribe(_ => {
      this.response = this.dropboxService.startMigration()
    })
  }
}

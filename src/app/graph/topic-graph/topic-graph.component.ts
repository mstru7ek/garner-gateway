import { AfterViewInit, Component, Input, OnInit, Type, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WordEntity } from 'app/model';
import { TopicService } from 'app/service';
import { ContextMenuComponent, ContextMenuResult, Logger, LoggerService } from 'app/shared';
import { GraphContext, NodeEntity } from 'app/shared/graph-widget/graph-context.class';
import { Observable, Subject } from 'rxjs';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class TopicGraphContextMenuComponent {

  @Input() node: NodeEntity

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-topic-graph',
  templateUrl: './topic-graph.component.html',
  styleUrls: ['./topic-graph.component.scss']
})
export class TopicGraphComponent implements OnInit, AfterViewInit {
  logger: Logger

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<TopicGraphContextMenuComponent>
  contextMenuContent: Type<any> = TopicGraphContextMenuComponent

  graphContext: Subject<GraphContext> = new Subject<GraphContext>()

  selectedWord: WordEntity = null
  topicWordList: Observable<WordEntity[]> = null
  topicId: number = null
  wordId: number = null

  graphContextData: GraphContext = null

  private selectedNodeEntity: NodeEntity

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private topicService: TopicService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('TopicViewComponent')
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {

    this.activatedRoute.paramMap.subscribe( param => {
      this.topicId = +param.get('id')
      this.wordId = +param.get('wId')
      this.topicWordList = this.topicService.get(this.topicId)

      this.topicWordList.subscribe(topicWordList => {
        this.graphContextData = new GraphContext()

        for (const topicWord of topicWordList) {
          this.graphContextData.nodes.push({
            id: topicWord.id,
            name: topicWord.name,
            translation: topicWord.translation,
            selected: topicWord.id === this.wordId
          })
          for (const siblingRef of topicWord.topic.siblingsRefs) {
            this.graphContextData.links.push({
              source: topicWord.id,
              target: siblingRef,
            })
          }
        }

        this.graphContext.next(this.graphContextData)
      })
    })
  }

  onSelectedNode(nodeEntity: NodeEntity) {
    this.selectedNodeEntity = nodeEntity

    this.contextMenu.open()
    this.contextMenu.componentInstance().node = nodeEntity
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedNodeEntity.id])
        this.router.navigateByUrl(requestUrlTree)
        return
      } else if ('MAP' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/map', this.topicId, 'word', this.selectedNodeEntity.id])
        this.router.navigateByUrl(requestUrlTree)
      } else {
        this.logger.error('Invalid context menu event action = ' + $event.action)
      }
    }
  }
}

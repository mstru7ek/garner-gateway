import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TopicGraphComponent, TopicGraphContextMenuComponent } from './topic-graph';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  entryComponents: [
    TopicGraphContextMenuComponent
  ],
  declarations: [
    TopicGraphComponent,
    TopicGraphContextMenuComponent
  ],
  exports: [
    TopicGraphComponent
  ]
})
export class GraphModule { }

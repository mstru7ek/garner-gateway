import { Component, OnInit, Type, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PreferenceEntity, TopicEntity, Page } from 'app/model';
import { PreferenceService, TopicService } from 'app/service';
import { ContextMenuComponent, ContextMenuResult, Logger, LoggerService } from 'app/shared';
import { Observable, BehaviorSubject } from 'rxjs';
import { flatMap } from 'rxjs/operators';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class TopicListContextMenuComponent {

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss']
})
export class TopicListComponent implements OnInit {

  private logger: Logger

  @ViewChild(ContextMenuComponent)
  contextMenu: ContextMenuComponent<TopicListContextMenuComponent>
  contextMenuContent: Type<any> = TopicListContextMenuComponent

  pageable: Observable<Page<TopicEntity>>
  pageNumber = new BehaviorSubject<number>(0)

  selectedTopic: TopicEntity
  namespaceTopics: Observable<TopicEntity[]> | null = null
  userPreference: Observable<PreferenceEntity> | null = null

  constructor(
    private router: Router,
    private topicService: TopicService,
    private preferenceService: PreferenceService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('TopicListComponent')
    this.pageable = this.pageNumber
      .pipe(
        flatMap(pageNo => this.topicService.list(pageNo))
      )
  }

  ngOnInit() {
    this.userPreference = this.preferenceService.getUserPreferences()
  }

  openContextMenu(topic: TopicEntity) {
    this.selectedTopic = topic
    this.contextMenu.open()
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/topic', this.selectedTopic.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.selectedTopic.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('MAP' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/map', this.selectedTopic.id])
        this.router.navigateByUrl(requestUrlTree)
      } else {
        this.logger.error('Undefined action in context menu', $event)
      }
    }
  }

}

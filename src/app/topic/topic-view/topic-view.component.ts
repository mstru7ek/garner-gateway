import { Router, ActivatedRoute } from '@angular/router'
import { Component, OnInit, ViewChild, Type } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

import { ContextMenuComponent, ContextMenuResult, LoggerService, Logger } from 'app/shared'
import { TopicService } from 'app/service'
import { WordEntity } from 'app/model'
import { Observable } from 'rxjs';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class TopicViewContextMenuComponent  {

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.scss']
})
export class TopicViewComponent implements OnInit {
  logger: Logger

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<TopicViewContextMenuComponent>
  contextMenuContent: Type<any> = TopicViewContextMenuComponent

  selectedWord: WordEntity = null
  topicWordList: Observable<WordEntity[]> = null
  topicId: number = null

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private topicService: TopicService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('TopicViewComponent')
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe( param => {
      this.topicId = +param.get('id')
      this.topicWordList = this.topicService.get(this.topicId)
    })
  }

  openContextMenu( word: WordEntity ) {
    this.selectedWord = word
    this.contextMenu.open()
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
        return
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.topicId, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('MAP' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/map', this.topicId, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      } else {
        this.logger.error('Invalid context menu event action = ' + $event.action)
      }
    }
  }

}

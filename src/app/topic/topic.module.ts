import { NgModule, } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'

import { SharedModule } from 'app/shared/shared.module'
import { ServiceModule } from 'app/service/service.module'

import {
  TopicViewComponent,
  TopicListComponent,
  TopicListContextMenuComponent,
  TopicViewContextMenuComponent,
} from './';

@NgModule({
  imports: [
    NgbModalModule,
    CommonModule,
    SharedModule,
    ServiceModule,
  ],
  declarations: [
    TopicViewComponent,
    TopicViewContextMenuComponent,
    TopicListComponent,
    TopicListContextMenuComponent,
  ],
  entryComponents: [
    TopicListContextMenuComponent,
    TopicViewContextMenuComponent,
  ],
  exports: [
    TopicViewComponent,
    TopicListComponent,
  ]
})
export class TopicModule { }

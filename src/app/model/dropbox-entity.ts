
export interface DbxRedirectUri {
    redirectUri: string
}

export interface DbxAccessToken {
    valid: boolean
}

export interface DbxAuthentication {
    success: boolean
}

export interface FileMetadata {
    valid: boolean
    errorMsg: string
    url: string
    id: string
    name: string

    wordId: number
    wordName: string
    namespaceId: number
    createdDate: Date
}

export interface DeleteFileMetedata {
  valid: boolean
  errorMsg: string
}

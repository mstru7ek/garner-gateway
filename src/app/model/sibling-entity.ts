export interface SiblingEntity {

    id: number
    active?: boolean
    name?: string
    translation?: string
}

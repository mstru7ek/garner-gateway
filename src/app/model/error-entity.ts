export interface ErrorEntity {

    code: string
    message: string
    target: string
}

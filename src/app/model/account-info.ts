import { Authorities } from 'app/model'

export interface AccountInfo {
    id?: number,
    login: string,
    firstName: string,
    lastName: string,
    email: string,
    imageUrl: string,
    activated: boolean,
    langKey: string,
    createdDate: string,
    authorities: Authorities[]
}

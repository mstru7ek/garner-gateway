import { NamespaceEntity } from 'app/model'

export interface PreferenceEntity {

    id?: number
    login: string
    password: string
    email: string
    namespace: NamespaceEntity
}

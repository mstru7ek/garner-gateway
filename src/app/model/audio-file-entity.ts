export interface AudioFileEntity {
    id: number
    name: string
    url: string
}

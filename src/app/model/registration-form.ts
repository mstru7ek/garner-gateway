
export class RegistrationForm {

    constructor(
        readonly firstName: string,
        readonly lastName: string,
        readonly login: string,
        readonly password: string,
        readonly email: string) { }
}

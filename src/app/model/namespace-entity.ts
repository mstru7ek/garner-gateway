
export interface NamespaceEntity {

    id?: number
    name: string
    active?: boolean
}

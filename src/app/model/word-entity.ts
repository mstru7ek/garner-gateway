
import { SiblingEntity } from 'app/model'

export interface WordEntity {
  id?: number
  active?: boolean
  name?: string
  translation?: string
  sentence?: string
  transcription?: string
  latitude?: number
  longitude?: number
  createdDate?: Date
  topic?: {
    id: number
    size?: number
    siblings?: SiblingEntity[]
    siblingsRefs?: number[]
    namespaceId: number
  }
}

export interface CoordinateEntity {
  latitude: number
  longitude: number
}

export interface SuggestWordEntity {
  id: number
  name: string
  translation: string
}

export interface UpdateWordEntity {
  name: string
  translation: string
  sentence: string
  transcription: string
}

export class CreateWordEntity {
  name: string
  translation: string
  sentence: string
  transcription: string
  latitude: number
  longitude: number
}

export interface WordCoordinateEntity {
  latitude: number
  longitude: number
}

export interface ProblemEntity {
  type: string
  title: string
  status: string
  detail: string
}

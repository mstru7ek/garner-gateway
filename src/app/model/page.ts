
export interface Page<T> {

  content: T[]

  first: Boolean
  last: Boolean
  number: number
  numberOfElements: number
  pageable: Pageable
  size: number
  sort: Sort
  totalElements: number
  totalPages: number
}

export interface Sort {
  sorted: Boolean
  unsorted: Boolean
}

export interface Pageable {
  offset: number
  pageNumber: number
  pageSize: number
  paged: Boolean
  sort: Sort
  unpaged: Boolean
}

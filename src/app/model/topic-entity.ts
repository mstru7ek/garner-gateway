export interface TopicEntity {
    id: number
    size: number
    wordsNames: string[]
}

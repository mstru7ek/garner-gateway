import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ErrorComponent, FooterComponent, MainComponent } from './';
import { LayoutRoutingModule } from './layout-routing.module';

@NgModule({
  declarations: [
    ErrorComponent,
    FooterComponent,
    MainComponent
  ],
  imports: [
    LayoutRoutingModule,
    SharedModule,
  ]
})
export class LayoutModule { }

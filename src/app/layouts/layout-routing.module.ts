import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { navbarRoute } from 'app/grn-app.route'
import { errorRoute } from './'
import { DEF_ROUTE_CONFIG } from 'app/shared';

const LAYOUT_ROUTES = [
    navbarRoute,
    ...errorRoute
]

@NgModule({
    imports: [
        RouterModule.forRoot(LAYOUT_ROUTES, DEF_ROUTE_CONFIG)
    ],
    exports: [
        RouterModule
    ]
})
export class LayoutRoutingModule {}

import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRouteSnapshot, NavigationEnd, ActivatedRoute } from '@angular/router'
import { Title } from '@angular/platform-browser'
import { Logger, LoggerService } from 'app/shared';

@Component({
  selector: 'grn-main',
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {

  private logger: Logger

  constructor(
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('DbxAccessGrantedComponent')
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'garnerFrontendApp'
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title
    }
    return title
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.titleService.setTitle(this.getPageTitle(this.router.routerState.snapshot.root))
      }
    })

    this.activatedRoute.queryParamMap.subscribe(queryParam => {
      if (queryParam.has('error')) {
        const dbxError = queryParam.get('error')
        this.logger.error(' Dbx connection error with code: ', dbxError)
        return
      }
      if (queryParam.get('state') && queryParam.get('code')) {
        const dbxState = queryParam.get('state')
        const dbxCode = queryParam.get('code')

        this.logger.error(' dbxCode: ', dbxCode)
        this.logger.error(' dbxState: ', dbxState)
      }
    })
  }
}

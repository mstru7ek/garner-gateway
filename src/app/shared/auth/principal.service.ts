import { Injectable } from '@angular/core';
import { Authorities } from 'app/model';
import { AccountInfo } from 'app/model/account-info';
import { AccountService } from 'app/shared/auth/account.service';
import { Observable, ReplaySubject, Subject } from 'rxjs';

@Injectable()
export class Principal {
  private userIdentity: AccountInfo
  private authenticated = false
  private authenticatedUser = new ReplaySubject<boolean>()
  private authenticationState = new Subject<AccountInfo>()

  constructor(
    private account: AccountService,
  ) {
    this.authenticatedUser.next(false)
  }

  authenticate(identity) {
    this.userIdentity = identity
    this.authenticated = identity !== null
    this.authenticatedUser.next(identity !== null)
    this.authenticationState.next(this.userIdentity)
  }

  hasAnyAuthority(authorities: Authorities[]): Promise<boolean> {
    return Promise.resolve(this.hasAnyAuthorityDirect(authorities))
  }

  hasAnyAuthorityDirect(authorities: Authorities[]): boolean {
    if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
      return false
    }
    for (let i = 0; i < authorities.length; i++) {
      if (this.userIdentity.authorities.indexOf(authorities[i]) !== -1) {
        return true
      }
    }
    return true
  }

  hasAuthority(authority: AccountInfo): Promise<boolean> {
    if (!this.authenticated) {
      return Promise.resolve(false)
    }
    return this.identity().then(id => {
      return Promise.resolve(id.authorities && id.authorities.indexOf(authority) !== -1)
    }, () => {
      return Promise.resolve(false)
    })
  }

  identity(force?: boolean): Promise<any> {
    if (force === true) {
      this.userIdentity = undefined
    }

    // check and see if we have retrieved the userIdentity data from the server.
    // if we have, reuse it by immediately resolving
    if (this.userIdentity) {
      return Promise.resolve(this.userIdentity)
    }

    // retrieve the userIdentity data from the server, update the identity object, and then resolve.
    return this.account.get().toPromise().then(account => {
      if (account) {
        this.userIdentity = account
        this.authenticated = true
        this.authenticatedUser.next(true)
      } else {
        this.userIdentity = null
        this.authenticated = false
        this.authenticatedUser.next(false)
      }
      this.authenticationState.next(this.userIdentity)
      return this.userIdentity
    }).catch(err => {
      if (err) { }
      this.userIdentity = null
      this.authenticated = false
      this.authenticatedUser.next(false)
      this.authenticationState.next(this.userIdentity)
      return null
    })
  }

  isAuthenticated(): boolean {
    return this.authenticated
  }

  isAuthenticatedUser(): Observable<boolean> {
    return this.authenticatedUser
  }

  isIdentityResolved(): boolean {
    return this.userIdentity !== undefined
  }

  getAuthenticationState(): Observable<any> {
    return this.authenticationState.asObservable()
  }

  getImageUrl(): String {
    return this.isIdentityResolved() ? this.userIdentity.imageUrl : null
  }
}

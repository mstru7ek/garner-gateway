
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage'
import { HttpClient,  HttpResponse } from '@angular/common/http'

@Injectable()
export class AuthServerProvider {
    constructor(
        private http: HttpClient,
        private $localStorage: LocalStorageService,
        private $sessionStorage: SessionStorageService
    ) {}

    getToken() {
        this.$localStorage.observe('authenticationToken').subscribe( val =>  {
          alert(val)
          console.warn('authenticationToken = ' + val)
        })
        return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken')
    }

    login(credentials): Observable<any> {

        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        }

        return this.http.post('/api/authenticate', data, { observe: 'response', responseType: 'text'}).pipe(map(authenticateSuccess.bind(this)))

        function authenticateSuccess(resp: HttpResponse<string>) {
            const bearerToken = resp.headers.get('Authorization')
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length)
                this.storeAuthenticationToken(jwt, credentials.rememberMe)
                return jwt
            }
        }
    }

    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe)
            return Promise.resolve(jwt)
        } else {
            return Promise.reject('auth-jwt-service Promise reject') // Put appropriate error message here
        }
    }

    storeAuthenticationToken(jwt, rememberMe) {
        if (rememberMe) {
            this.$localStorage.store('authenticationToken', jwt)
        } else {
            this.$sessionStorage.store('authenticationToken', jwt)
        }
    }

    logout(): Observable<any> {
        return new Observable(observer => {
            this.$localStorage.clear('authenticationToken')
            this.$sessionStorage.clear('authenticationToken')
            this.$sessionStorage.clear('previousUrl')
            observer.complete()
        })
    }
}

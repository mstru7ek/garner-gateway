import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Authorities } from 'app/model';
import { Principal } from 'app/shared/auth/principal.service';
import { StateStorageService } from 'app/shared/auth/state-storage.service';

const _log = console

@Injectable()
export class UserRouteAccessService implements CanActivate {

    constructor(private router: Router,
                private principal: Principal,
                private stateStorageService: StateStorageService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {

        const authorities = route.data['authorities']
        if (!authorities || authorities.length === 0) {
            return true
        }

        return this.checkLogin(authorities, state.url)
    }

    checkLogin(authorities: string[], url: string): Promise<boolean> {
        const principal = this.principal
        return Promise.resolve(principal.identity().then(account => {
            if (account && principal.hasAnyAuthorityDirect(authorities as Authorities[])) {
                return true
            }
            this.stateStorageService.storeUrl(url)
            this.router.navigate(['accessdenied']).then(() => {
                // only show the login dialog, if the user hasn't logged in yet
                _log.warn('accessdenied')
                if (!account) {
                    _log.warn('accessdenied : login')
                    this.router.navigate(['login'])
                }
            })
            return false
        }))
    }
}

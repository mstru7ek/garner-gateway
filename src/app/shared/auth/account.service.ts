import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { AccountInfo } from 'app/model/account-info'
import { Observable } from 'rxjs'
import { RegistrationForm } from '../../model/registration-form';

@Injectable()
export class AccountService  {
    constructor(private http: HttpClient) { }

    get(): Observable<AccountInfo> {
        return this.http.get<AccountInfo>('/api/account')
    }

    save(account: AccountInfo): Observable<Object> {
        return this.http.post('/api/account', account)
    }

    register(registrationForm: RegistrationForm): Observable<Object> {
        return this.http.post('/api/register', registrationForm)
    }

    activate(key: string): Observable<Object> {
        return this.http.get('/api/activate?key=' + key)
    }

}

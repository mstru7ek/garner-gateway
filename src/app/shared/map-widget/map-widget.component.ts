import { Component, ElementRef, EventEmitter, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { MAP_TILES, defaultIcon } from 'app/shared'

import * as L from 'leaflet'
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

@Component({
  selector: 'grn-map-widget',
  templateUrl: './map-widget.component.html',
  styleUrls: ['./map-widget.component.scss']
})
export class MapWidgetComponent implements OnInit {

  @Input() lat = 52.212
  @Input() lng = 21.020
  @Output() position: EventEmitter<number[]> = new EventEmitter()
  @ViewChild('map') private _mapRef: ElementRef

  isEditMode = false

  private map: L.Map
  private marker: L.Marker
  private prevBounds: L.LatLngBounds
  private initPosition: L.LatLng

  constructor(
    private _zone: NgZone
  ) { }

  ngOnInit() {

    this.map = L.map(this._mapRef.nativeElement, {
      center: [this.lat, this.lng],
      zoom: 16
    })

    MAP_TILES.build().addTo(this.map);

    this.map.on('move', _ => {
      this._zone.run( () => {
        if (this.isEditMode) {
          this.marker.setLatLng(this.map.getCenter())
          this.lat = this.marker.getLatLng().lat
          this.lng = this.marker.getLatLng().lng
        }
      })
    })

    this.lockMapEvents()

    this.marker = L.marker([this.lat, this.lng], {
      icon: defaultIcon
    })

    this.marker.addTo(this.map)

  }

  onChangePosition($event: MouseEvent) {

    this.initPosition = this.marker.getLatLng()
    this.prevBounds = this.map.getBounds()

    this.isEditMode = true
    this.unlockMapEvents()
  }

  onCancel($event: MouseEvent) {

    this.marker.setLatLng(this.initPosition)
    this.map.fitBounds(this.prevBounds)

    this.isEditMode = false
    this.lockMapEvents()
  }

  onSave($event: MouseEvent) {
    this.isEditMode = false
    this.lockMapEvents()

    this.initPosition = this.marker.getLatLng()

    const pos = this.marker.getLatLng()
    this.position.emit([pos.lat, pos.lng])
  }

  private lockMapEvents() {
    this.map.dragging.disable()
    this.map.boxZoom.disable()
    this.map.doubleClickZoom.disable()
  }

  private unlockMapEvents() {
    this.map.dragging.enable()
    this.map.boxZoom.enable()
    this.map.doubleClickZoom.enable()
  }
}

import { Component, OnInit, Type, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ContextMenuComponent } from 'app/shared/context-menu/context-menu.component';
import { ErrorService } from 'app/shared/error.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class ErrorWidgetContextMenuComponent  {

  messages: string[] = []

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-error-widget',
  templateUrl: './error-widget.component.html',
  styleUrls: ['./error-widget.component.scss']
})
export class ErrorWidgetComponent implements OnInit {

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<ErrorWidgetContextMenuComponent>
  contextMenuContent: Type<any> = ErrorWidgetContextMenuComponent

  searchText: BehaviorSubject<string> = new BehaviorSubject('')

  constructor(
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.errorService.getHttpErrorEvent().subscribe(event => {
      if ( !this.contextMenu.isOpen()) {
        this.contextMenu.open()
      }
      this.contextMenu.componentInstance().messages.push(event.message)
    } )
  }

  selectedItem($selectedItem) {
    // if ($event) {
    //   this.contextMenu.componentInstance().messages = []
    // }
  }
}

import { Component, OnInit,  Input, ElementRef, TemplateRef } from '@angular/core';
import { Observable, fromEvent, BehaviorSubject } from 'rxjs';
import { Page } from 'app/model';

/**
 * Parametrized inifinity scroll component.
 */
@Component({
    selector: 'grn-inifinity',
    templateUrl: './inifinity.component.html',
    styles: []
})
export class InifinityComponent<T> implements OnInit {

    @Input()
    pageNumber: BehaviorSubject<number>;

    @Input()
    pageable: Observable<Page<T>>;

    @Input()
    body: TemplateRef<any>;

    @Input()
    triggerAtPercent = 0.1;

    @Input()
    clear = new BehaviorSubject<boolean>(false);

    private items: T[] = [];
    private lastPage: Page<T>;
    private loadingNext: Boolean = true;

    constructor(readonly element: ElementRef) { }

    ngOnInit() {
        this.pageable.subscribe(this.onNextPage.bind(this));
        fromEvent(document, 'scroll').subscribe(this.onScrollEvent.bind(this));
        this.clear.subscribe(this.clearContext.bind(this))
    }

    onScrollEvent(event: Event) {
        if (!this.loadingNext && !this.lastPage.last) {

            const c = this.element.nativeElement.getBoundingClientRect();

            const triggerAtItem = this.triggerAtPercent * this.lastPage.size;
            const avgItemHeight = c.height / this.items.length;

            const itemYPosition = c.height - triggerAtItem * avgItemHeight;
            const bottom = - c.y + window.innerHeight;

            if (bottom > itemYPosition) {
                this.loadingNext = true;
                this.pageNumber.next(this.lastPage.number + 1);
            }
        }
    }

    clearContext(state: boolean) {
        if (state) { this.items = [] }
    }

    onNextPage(page: Page<T>) {
        this.lastPage = page;
        this.loadingNext = false;
        this.items = this.items.concat(page.content);
    }

    context() {
        return {
            'items': this.items,
            'loadingNext': this.loadingNext
        };
    }
}

import { AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';
import { BaseType } from 'd3';
import { Selection } from 'd3-selection';
import { Observable } from 'rxjs';
import { GraphContext, LinkEntity, NodeEntity } from './graph-context.class';

type NodeEntitySelection = Selection<BaseType, NodeEntity, BaseType, {}>
type LinkEntitySelection = Selection<BaseType, LinkEntity, BaseType, {}>
type HTMLElementSelection = Selection<BaseType, {}, HTMLElement, any>
type DragBehaviorFunctionType = (selection: Selection<BaseType, NodeEntity, BaseType, {}>, ...args: any[]) => void
type ForceLinkType = d3.ForceLink<{}, d3.SimulationLinkDatum<{}>>
type SimulationType = d3.Simulation<{}, undefined>

@Component({
  selector: 'grn-graph-widget',
  templateUrl: './graph-widget.component.html',
  styleUrls: ['./graph-widget.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GraphWidgetComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() graphContext: Observable<GraphContext>
  @Output() selectedNode = new EventEmitter<NodeEntity>()

  private forceLink: ForceLinkType
  private simulation: SimulationType

  private mainGroup: HTMLElementSelection
  private bbRect: HTMLElementSelection
  private zoom: d3.ZoomBehavior<Element, {}>

  private svg: Selection<any, {}, HTMLElement, any>

  private _DEBUG_ = false

  constructor(
    private _zone: NgZone,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.graphContext.subscribe(context => {
      this._zone.run(() => {
        this.setupForceSimulation()
        this.rebuildGraph(context)
      })
    })
  }

  ngOnDestroy(): void {
    d3.select(window)
      .on('resize', null)
      .on('orientationchange', null)
      .on('mouseup', null)
  }

  setupForceSimulation() {

    this.svg = d3.select('svg#graph')
    this.zoom = d3.zoom()

    this.forceLink = d3.forceLink<NodeEntity, LinkEntity>()
      .id((d: NodeEntity) => d.id.toString())
      .distance(() => 100)

    this.updateGraphViewport()

    this.mainGroup = this.svg.append('g')
      .attr('id', 'mainGroup')

    this.svg.call(this.zoom)

    /* RESIZE & ORIENTATION CHANGE*/
    d3.select(window)
      .on('resize', () => this.updateGraphViewportAdjustCenter())
      .on('orientationchange', () => this.updateGraphViewportAdjustCenter())

    this.zoom.on('zoom', () => { this.mainGroup.attr('transform', d3.event.transform) })
    this.zoom.on('start', function() { d3.zoomTransform(this) })
    this.zoom.on('end', function() { d3.zoomTransform(this) })

    this.simulation = d3.forceSimulation()
      .force('link', this.forceLink)
      .force('charge', d3.forceManyBody()
        .strength(() => -300))

    this.simulation.alphaMin(0.25)
    this.simulation.on('end', () => this.adjustAndCenter())

    this.bbRect = this.mainGroup.append('rect').attr('class', 'bounding-rect')
    if (this._DEBUG_ === false) {
      this.bbRect.style('display', 'none')
    }
  }

  rebuildGraph(context: GraphContext) {

    this.simulation.stop()

    /* CLEAR CANVAS */
    this.mainGroup.selectAll('g').remove()

    /* BUILD PATHS */
    const path = this.mainGroup.append('g').selectAll('path')
      .data(context.links)
      .enter()
      .append('path')
      .attr('class', () => 'link')

    /* BUILD NODE TEXT NAME */
    const text = this.mainGroup.append('g').selectAll('text')
      .data(context.nodes)
      .enter()
      .append('text')
      .attr('x', 15)
      .attr('y', '.31em')
      .attr('nodeId', (d: NodeEntity) => d.id)
      .text((d: NodeEntity) => d.name)

    /* BUILD CIRCLES */
    const circle = this.mainGroup.append('g').selectAll('circle')
      .data(context.nodes)
      .enter()
      .append('circle')
      .attr('r', 10)
      .attr('class', (d: NodeEntity) => d.selected ? 'nodeParent' : 'node')
      .attr('nodeId', (d: NodeEntity) => d.id)
      .call(
        <DragBehaviorFunctionType>d3.drag()
          .on('start', () => this.dragstarted())
          .on('drag', () => this.dragged())
          .on('end', () => this.dragended())
      )

    /* Circle color , on click , on mouseup */
    const $this: this = this
    this.svg.selectAll('circle').on('click', function(node: NodeEntity) {
      d3.select(this).classed('nodeSelected', true)
      $this.selectedNode.emit(node)
    })

    d3.select(window).on('mouseup', () => {
      this.mainGroup.selectAll('circle').classed('nodeSelected', false)
      this.mainGroup.selectAll('circle').filter((d: NodeEntity) => d.selected).classed('nodeParent', true)
      this.adjustAndCenter()
    })

    /* START SIMULATION */
    this.simulation.nodes(context.nodes)
      .on('tick', () => this.tick(path, text, circle))

    this.forceLink.links(context.links)
    this.simulation.restart()

    this.adjustAndCenter(50)
  }

  // /* Drag handlers */
  dragstarted(this: GraphWidgetComponent) {

    if (!d3.event.active) {
      this.simulation.alphaTarget(0.5).restart()
    }
    d3.event.subject.fx = d3.event.subject.x
    d3.event.subject.fy = d3.event.subject.y
  }

  dragged(this: GraphWidgetComponent) {
    d3.event.subject.fx = d3.event.x
    d3.event.subject.fy = d3.event.y
  }

  dragended(this: GraphWidgetComponent) {

    if (!d3.event.active) {
      this.simulation.alphaTarget(0)
    }
    d3.event.subject.fx = null
    d3.event.subject.fy = null
  }

  graphViewport(): ClientRect {
    return document.querySelector('svg#graph').getBoundingClientRect()
  }

  targetTransform() {

    const brect = this.getBoundingBoxRect(this.mainGroup)
    const graphViewport = this.graphViewport()

    const scale = Math.min(
      1,
      Math.min(1, 0.98 / Math.max(brect.width / graphViewport.width, brect.height / graphViewport.height))
    )

    const translate = [
      graphViewport.width / 2 - (brect.width / 2 + brect.x) * scale,
      graphViewport.height / 2 - (brect.height / 2 + brect.y) * scale
    ]

    return d3.zoomIdentity.translate(translate[0], translate[1]).scale(scale)
  }

  adjustAndCenter(duration?: number) {
    this.svg.transition()
      .duration(duration ? duration : 350)
      .call(this.zoom.transform, this.targetTransform())
  }

  tick(path: LinkEntitySelection, text: NodeEntitySelection, circle: NodeEntitySelection) {

    path.attr('d', this.linkLineTo)
    text.attr('transform', this.transform)
    circle.attr('transform', this.transform)

    if (this._DEBUG_) {
      const boundingBox = this.getBoundingBoxRect(this.mainGroup)
      this.bbRect.attr('x', boundingBox.x)
      this.bbRect.attr('y', boundingBox.y)
      this.bbRect.attr('width', boundingBox.width)
      this.bbRect.attr('height', boundingBox.height)
    }
  }

  linkLineTo(d: LinkEntity) { // LinkEntity
    const source: NodeEntity = <NodeEntity>d.source
    const target: NodeEntity = <NodeEntity>d.target
    return 'M' + source.x + ',' + source.y + 'L' + target.x + ',' + target.y
  }

  transform(d: NodeEntity) {
    return 'translate(' + d.x + ',' + d.y + ')'
  }

  updateGraphViewportAdjustCenter() {

    this.updateGraphViewport()
    this.adjustAndCenter()
  }

  updateGraphViewport() {
    const graphViewport = this.graphViewport()

    this.svg
      .attr('width', graphViewport.width)
      .attr('height', graphViewport.height)
  }

  getBoundingBoxRect(mainGroup: HTMLElementSelection): SVGRect {
    // SELECT ALL CHILD NODES EXCEPT THE BOUNDING RECT
    const allChildNodes = mainGroup.selectAll('g').nodes()

    const x = d3.min(allChildNodes, (d: SVGGraphicsElement) => d.getBBox().x)
    const y = d3.min(allChildNodes, (d: SVGGraphicsElement) => d.getBBox().y)
    const width = d3.max(allChildNodes, (d: SVGGraphicsElement) => (d.getBBox().x + d.getBBox().width) - x)
    const height = d3.max(allChildNodes, (d: SVGGraphicsElement) => (d.getBBox().y + d.getBBox().height) - y)

    return { x, y, width, height } as SVGRect
  }
}

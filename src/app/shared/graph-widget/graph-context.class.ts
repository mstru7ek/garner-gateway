import { SimulationNodeDatum, SimulationLinkDatum } from 'd3'

export interface NodeEntity extends SimulationNodeDatum {
  id: number,
  name: string,
  translation: string,
  selected: boolean,
}

export type LinkEntity = SimulationLinkDatum<NodeEntity>

export class GraphContext {
  constructor(
    public links: LinkEntity[] = [],
    public nodes: NodeEntity[] = [],
  ) { }
}

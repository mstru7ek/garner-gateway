import { Component } from '@angular/core';
import { Principal } from 'app/shared/auth';
import { Observable } from 'rxjs';

@Component({
  selector: 'grn-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {

  isAuthenticatedState: Observable<boolean> = null

  constructor(private principal: Principal) {
    this.isAuthenticatedState = this.principal.isAuthenticatedUser()
  }
}

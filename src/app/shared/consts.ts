import { ExtraOptions } from '@angular/router';

export const DEF_ROUTE_CONFIG: ExtraOptions = {
  useHash: true,
  enableTracing: false,
  scrollPositionRestoration: 'enabled', // !!! nie rozpoznane
  anchorScrolling: 'enabled'
 }

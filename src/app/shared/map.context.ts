import * as L from 'leaflet'

const  _defaultIcon = L.icon({
  iconUrl: 'images/marker-icon.png',
  iconSize: [25, 41],
  iconAnchor: [12, 40],
  popupAnchor: [13, -2],
  shadowUrl: 'images/marker-shadow.png',
  shadowSize: [41, 41],
  shadowAnchor: [12, 40]
})

const _MAP_TILES = {

  // MAPBOX access token and url
  //
  // https://docs.mapbox.com/api/maps/#mapbox-styles
  //
  // //id: 'streets-v11',

  ACCESS_TOKEN: 'pk.eyJ1IjoibXN0cnV6ZWsiLCJhIjoiY2sxOWliaTRsMDVwczNtbnR3bTFwaml1dSJ9.mX_htMIvPXvPJMR8BNuDVg',

  ACCOUNT :  'mstruzek',

  build: function() {
    return L.tileLayer('https://api.mapbox.com/styles/v1/' + this.ACCOUNT + '/{id}/tiles/256/{z}/{x}/{y}?access_token=' + this.ACCESS_TOKEN, {
        id: 'ck1ait32k03h91cpihc8wdedt',
        attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    })
  }
}

export const MAP_TILES = _MAP_TILES
export const defaultIcon = _defaultIcon

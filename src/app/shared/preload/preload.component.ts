import { Component, OnInit } from '@angular/core';
import { PreloadService } from 'app/shared/preload.service';

@Component({
  selector: 'grn-preload',
  templateUrl: './preload.component.html',
  styleUrls: ['./preload.component.scss']
})
export class PreloadComponent implements OnInit {

  visible = true

  constructor(
    private preloadService: PreloadService
  ) { }

  ngOnInit() {
    this.preloadService.getPreloadState().subscribe( state => {
      this.visible = state !== 'SHOW'
    })
  }

}

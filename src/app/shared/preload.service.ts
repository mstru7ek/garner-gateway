import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class PreloadService {

  preloadState: Subject<string> = new Subject<string>()

  constructor() { }

  getPreloadState(): Observable<string> {
    return this.preloadState
  }

  show(): void {
    this.preloadState.next('SHOW')
  }

  hide(): void {
    this.preloadState.next('HIDE')
  }

}

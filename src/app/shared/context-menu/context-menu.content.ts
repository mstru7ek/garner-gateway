import { Component } from '@angular/core'

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
    selector: 'grn-context-menu-content',
    templateUrl: './context-menu.content.html',
    styleUrls: ['./context-menu.content.scss']
})
export class ContextMenuContent {

    constructor(
        public activeModal: NgbActiveModal
    ) { }
}

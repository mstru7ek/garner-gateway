import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ContextMenuContent } from './context-menu.content';

export class ContextMenuResult {
  constructor(
    public readonly selected: Boolean,
    public readonly action: string,
  ) {}
}

@Component({
  selector: 'grn-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContextMenuComponent<T> {

  @Input() content: T
  @Output() selected = new EventEmitter<ContextMenuResult>()

  modalRef: NgbModalRef

  isVisible = false

  constructor(private modalService: NgbModal) { }

  open() {
    const contentRef = this.content != null ? this.content : ContextMenuContent

    this.modalRef = this.modalService.open(contentRef, {
      container: '#modalContentRef'
    })

    this.isVisible = true

    this.modalRef.result.then(result => {
      this.isVisible = false
      this.selected.emit(new ContextMenuResult(true, result))
    }, reason => {
      this.isVisible = false
      if (reason === ModalDismissReasons.ESC) {
        this.selected.emit(new ContextMenuResult(false, 'MODAL_DISMISS_ESC') )
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        this.selected.emit(new ContextMenuResult(false, 'MODAL_DISMISS_BACKDROP_CLICK'))
      } else {
        throw new Error(`MODAL_DISMISS_ERROR - reason : ${reason}`)
      }
    })
  }

  isOpen(): boolean {
    return this.isVisible
  }

  public componentInstance(): T {
    return this.modalRef != null ? ('componentInstance' in this.modalRef  ? this.modalRef.componentInstance : null) : null
  }
}

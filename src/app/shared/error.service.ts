import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ErrorEntity, ProblemEntity } from 'app/model';
import { EventManager, HttpErrorServiceEvent } from 'app/service';
import { Logger, LoggerService } from 'app/shared/logger.service';
import { PreloadService } from 'app/shared/preload.service';
import { Observable ,  Subject } from 'rxjs';

export interface HttpErrorEvent {
  message: string
}

@Injectable()
export class ErrorService {

  logger: Logger

  httpErrorEvent: Subject<HttpErrorEvent> = new Subject<HttpErrorEvent>()

  constructor(
    private eventManager: EventManager,
    private preloadService: PreloadService,
    private loggerService: LoggerService,
  ) {

    this.logger = this.loggerService.getInstance('ErrorService')

    this.eventManager.subscribe('http.service.error', (errorServiceEvent: HttpErrorServiceEvent) => {
      this.preloadService.hide()
      const errorResponse: HttpErrorResponse = errorServiceEvent.errorResponse

      if (errorResponse.url.endsWith('/api/account')) {
        const problem: ProblemEntity = errorResponse.error
        this.logProblemEntity(problem)
        return
      }

      if (errorResponse.error.description) {
        this.logHttpErrorResponse(errorResponse)
        this.httpErrorEvent.next({
          message: errorResponse.error.description
        })
        return
      }

      if (instanceOfErrorEntity(errorResponse.error)) {
        const errorEntity: ErrorEntity = errorResponse.error
        this.logGrnErrorEntity(errorEntity)
        this.httpErrorEvent.next({
          message: errorEntity.message
        })
        return
      }

      if (instanceOfProblemEntity(errorResponse.error)) {
        const problem: ProblemEntity = errorResponse.error
        this.logProblemEntity(problem)
        this.httpErrorEvent.next({
          message: problem.detail || problem.title
        })
        return
      }

      this.logHttpErrorResponse(errorResponse)
      this.httpErrorEvent.next({
        message: 'Service unavailable. ' + errorResponse.statusText
      })

    })
  }

  logHttpErrorResponse(errorResponse: HttpErrorResponse) {
    this.logger.warn('---------------------------HttpErrorResponse--------------------------------------')
    this.logger.warn('errorResponse.type    = ', errorResponse.type)
    this.logger.warn('errorResponse.url     = ', errorResponse.url)
    this.logger.warn('errorResponse.status  = ', errorResponse.status)
    this.logger.warn('')
    for (const header of errorResponse.headers.keys()) {
      this.logger.warn('errorResponse.header[' + header + '] = ', errorResponse.headers.get(header))
    }
    this.logger.warn('')
    this.logger.warn('errorResponse.name      = ', errorResponse.name)
    this.logger.warn('errorResponse.message   = ', errorResponse.message)
    this.logger.warn('errorResponse.error     = ', errorResponse.error)
    this.logger.warn('--------------------------------------------------------------------------------')
  }

  logGrnErrorEntity(errorEntity: ErrorEntity) {
    this.logger.log('----------------------------Grn Error Entity-------------------------------------')
    this.logger.log('code      = ', errorEntity.code)
    this.logger.log('message   = ', errorEntity.message)
    this.logger.log('target    = ', errorEntity.target)
    this.logger.log('---------------------------------------------------------------------------------')
  }

  logProblemEntity(problem: ProblemEntity) {
    this.logger.log('----------------------------     Problem    -------------------------------------')
    this.logger.log('type      = ', problem.type)
    this.logger.log('title     = ', problem.title)
    this.logger.log('status    = ', problem.status)
    this.logger.log('detail    = ', problem.detail)
    this.logger.log('---------------------------------------------------------------------------------')
  }

  getHttpErrorEvent(): Observable<HttpErrorEvent> {
    return this.httpErrorEvent
  }

}

export function instanceOfErrorEntity(object: any): object is ErrorEntity {
  if ( typeof object  === 'string') {
    return false
  }
  return 'code' in object && 'message' in object && 'target' in object
}

export function instanceOfProblemEntity(object: any): object is ProblemEntity {
  if ( typeof object  === 'string') {
    return false
  }
  return 'type' in object && 'title' in object && 'status' in object
}

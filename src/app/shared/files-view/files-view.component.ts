import { Component, Input, OnInit, ViewChild, Type } from '@angular/core';
import { FileMetadata } from 'app/model';
import { __asyncDelegator } from 'tslib';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ContextMenuComponent, ContextMenuResult } from '../context-menu/context-menu.component';
import { LoggerService, Logger } from '../logger.service';
import { FileService } from 'app/service';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class ImageContextMenuComponent {

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-files-view',
  templateUrl: './files-view.component.html',
  styleUrls: ['./files-view.component.scss'],
  entryComponents: [
    ImageContextMenuComponent
  ]

})
export class FilesViewComponent implements OnInit {

  logger: Logger

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<ImageContextMenuComponent>
  contextMenuContent: Type<any> = ImageContextMenuComponent

  private selectedImg: HTMLImageElement

  @Input() files: FileMetadata[]

  constructor(
    private fileService: FileService,
    loggerService: LoggerService
  ) {
    this.logger = loggerService.getInstance('FilesViewComponent')
  }

  ngOnInit() {
  }

  openContextMenu($event: Event) {
    this.selectedImg = <HTMLImageElement>$event.target
    this.addHighlight()
    this.contextMenu.open()
  }

  selectedItem($event: ContextMenuResult) {

    if ($event.selected) {
      if ('DELETE' === $event.action) {
        this.logger.log('Invalid context menu event action' + this.selectedImg.src)
        this.fileService.deleteImage(this.selectedImg.src).subscribe( _ => {
            this.selectedImg.remove()
        })
      } else {
        this.logger.error('Invalid context menu event action = ' + $event.action)
      }
    }
    this.delHighlight()
  }

  addHighlight() {
    this.selectedImg.classList.add('border')
    this.selectedImg.classList.add('border-warning')
  }

  delHighlight() {
    this.selectedImg.classList.remove('border')
    this.selectedImg.classList.remove('border-warning')
  }

}

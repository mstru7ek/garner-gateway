import { NgModule, } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'

import {ReactiveFormsModule} from '@angular/forms';

import { NgbTypeaheadModule, NgbCollapseModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap'

import { Ng2Webstorage } from 'ngx-webstorage';

import { ServiceModule } from 'app/service/service.module';
import {
    LoggerService,
    NavBarComponent,
    SearchNavComponent,
    ContextMenuComponent,
    ContextMenuContent,
    ScrollTopComponent,
    AudioFabComponent,
    MapWidgetComponent,
    SiblingsAutocompleteComponent,
    GraphWidgetComponent,
    PreloadComponent,
    UploadFileComponent,
    FilesViewComponent,
    ErrorWidgetComponent,
    PreloadService,
    ErrorWidgetContextMenuComponent,
    ErrorService,
    ImageContextMenuComponent,
    InifinityComponent,
    BrakePipe,
} from './'

import {
    AccountService,
    AuthServerProvider,
    Principal,
    StateStorageService,
    UserRouteAccessService
} from 'app/shared/auth';

@NgModule({
    imports: [
        NgbModalModule,
        NgbCollapseModule,
        NgbTypeaheadModule,
        Ng2Webstorage,
        CommonModule,
        FormsModule,
        ServiceModule,
        RouterModule,
        ReactiveFormsModule,
    ],
    declarations: [
        BrakePipe,
        NavBarComponent,
        SearchNavComponent,
        ContextMenuContent,
        ContextMenuComponent,
        ScrollTopComponent,
        AudioFabComponent,
        MapWidgetComponent,
        SiblingsAutocompleteComponent,
        GraphWidgetComponent,
        PreloadComponent,
        UploadFileComponent,
        FilesViewComponent,
        ErrorWidgetComponent,
        ErrorWidgetContextMenuComponent,
        ImageContextMenuComponent,
        InifinityComponent,
    ],
    entryComponents: [
        ContextMenuComponent,
        ErrorWidgetContextMenuComponent,
        ImageContextMenuComponent,
    ],
    exports: [
        BrakePipe,
        PreloadComponent,
        NavBarComponent,
        SearchNavComponent,
        ContextMenuComponent,
        ScrollTopComponent,
        AudioFabComponent,
        MapWidgetComponent,
        GraphWidgetComponent,
        SiblingsAutocompleteComponent,
        UploadFileComponent,
        FilesViewComponent,
        ErrorWidgetComponent,
        InifinityComponent
    ],
    providers: [
        // internal dep
        LoggerService,
        PreloadService,
        ErrorService,
        AccountService,
        AuthServerProvider,
        Principal,
        StateStorageService,
        UserRouteAccessService
    ]
})
export class SharedModule { }

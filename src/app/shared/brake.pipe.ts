import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'brake' })
export class BrakePipe implements PipeTransform {
  transform(value: String, ...args: any[]) {
    return value.replace(/\./g, '.<br/>')
  }

}

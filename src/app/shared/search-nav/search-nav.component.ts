import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'grn-search-nav',
  templateUrl: './search-nav.component.html',
  styleUrls: ['./search-nav.component.scss']
})
export class SearchNavComponent implements OnInit {

  searchText: String = ''

  @Output() textChanged: EventEmitter<String> = new EventEmitter()

  constructor() { }

  ngOnInit() {

  }

  onChanged(_event: MouseEvent) {
    this.textChanged.emit(this.searchText)
  }

  onClear(_event: MouseEvent) {
    this.searchText = ''
    this.textChanged.emit('')
  }
}

import { Injectable } from '@angular/core'

const _console = console

@Injectable()
export class LoggerService {

  private loggerInstance: { [key: string]: Logger } = {};

  constructor() {}

  getInstance(loggerName: string): Logger {
    if ( this.loggerInstance[loggerName] == null ) {
      this.loggerInstance[loggerName] = new Logger(loggerName)
    }
    return this.loggerInstance[loggerName]
  }
}

export class Logger {

  constructor(private loggerName: string) {
  }

  instance(): string {
    return this.loggerName + ' : '
  }

  trace(message?: any, ...optionalParams: any[]): void {
    if (optionalParams.length) {
      _console.trace( this.instance() + message, optionalParams)
    } else {
      _console.trace( this.instance() + message)
    }
  }

  log(message?: any, ...optionalParams: any[]): void {
    if (optionalParams.length) {
      _console.log( this.instance() + message, optionalParams)
    } else {
      _console.log( this.instance() + message)
    }
  }

  info(message?: any, ...optionalParams: any[]): void {
    if (optionalParams.length) {
      _console.info( this.instance() + message, optionalParams)
    } else {
      _console.info( this.instance() + message)
    }
  }

  warn(message?: any, ...optionalParams: any[]): void {
    if (optionalParams.length) {
      _console.warn( this.instance() + message, optionalParams)
    } else {
      _console.warn( this.instance() + message)
    }
  }

  error(message?: any, ...optionalParams: any[]): void {
    if (optionalParams.length) {
      _console.error( this.instance() + message, optionalParams)
    } else {
      _console.error( this.instance() + message)
    }
  }
}

import { Component, Input } from '@angular/core';
import { Logger, LoggerService } from 'app/shared/logger.service';

@Component({
  selector: 'grn-audio-fab',
  templateUrl: './audio-fab.component.html',
  styleUrls: ['./audio-fab.component.scss']
})
export class AudioFabComponent {

  @Input() name: string
  @Input() url: string

  private log: Logger

  private audio: HTMLMediaElement

  constructor(
    private loggerService: LoggerService,
  ) {
    this.log = this.loggerService.getInstance('AudioFabComponent')
  }

  onPlaySound() {
    if (this.audio == null) {
      this.audio = new Audio(this.url)
      this.audio.hidden = true
      this.audio.addEventListener('ended', () => {
        this.audio.pause()
        this.audio.currentTime = 0.0
      })
      this.audio.load()
    }

    this.audio.play().then(() => {
      this.log.warn(`Play...`)
    }, rejectedReason => {
      this.log.warn(`reason : ${rejectedReason}`)
      if (this.audio.error.code === MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED) {
        const error = `${rejectedReason} , context \n` +
          `- name = '${this.name}' \n` +
          `- url = '${this.url}' \n`
        this.log.error(error)
        this.audio = null
      }
    })
  }
}

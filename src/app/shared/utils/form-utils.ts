import { FormGroup } from '@angular/forms'

export class FormUtils {

     /**
      * Build validation messages
      */
    static buildValidationErrors(
        formGroup: FormGroup,
        formErrors: { [key: string]: string; },
        validationMessages: { [key: string]: { [key: string]: string; } }
      ): { [key: string]: string; } {

        for (const field in formErrors) {
          // clear previous error message (if any)
          formErrors[field] = ''
          const control = formGroup.get(field)
          if (control && control.dirty && !control.valid) {
            const messages = validationMessages[field]
            for (const key in control.errors) {
              formErrors[field] += messages[key] + ' '
            }
          }
        }
        return formErrors
      }
}

import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { SuggestWordEntity } from 'app/model';
import { SiblingService } from 'app/service';
import { Observable, of as observableOf } from 'rxjs';
import { debounceTime, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'grn-siblings-autocomplete',
  templateUrl: './siblings-autocomplete.component.html',
  styleUrls: ['./siblings-autocomplete.component.scss']
})
export class SiblingsAutocompleteComponent implements OnInit {

  @Output() result: EventEmitter<SuggestWordEntity> = new EventEmitter<SuggestWordEntity>()
  @ViewChild(NgbTypeahead) ngbTypeahead: NgbTypeahead

  wordId: number = null
  selectedItem: SuggestWordEntity
  searchSibling: (term: Observable<string>) => Observable<SuggestWordEntity[]>
  formatter = (x: SuggestWordEntity) => x.name

  constructor(
    private activatedRoute: ActivatedRoute,
    private siblingService: SiblingService,
  ) { }

  ngOnInit() {
    this.wordId = +this.activatedRoute.snapshot.paramMap.get('id')
    this.searchSibling = term =>
      term.pipe(debounceTime(200),
        filter(value => value.length >= 3),
        switchMap(searchText => {
          if (searchText) {
            return this.siblingService.suggestWord(this.wordId, searchText)
          }
          return observableOf()
        }))
  }

  emitSelectedItem() {
    this.result.emit(this.selectedItem)
    this.selectedItem = null
    this.ngbTypeahead.dismissPopup()
  }

  dismissValue() {
    this.selectedItem = null
    this.ngbTypeahead.dismissPopup()
  }
}

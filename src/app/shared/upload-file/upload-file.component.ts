import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { PreferenceEntity, WordEntity, NamespaceEntity } from 'app/model';
import { FileService, PreferenceService } from 'app/service';
import { EventManager } from 'app/service/event-manager.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'grn-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  file: File

  @Input() wordEntity: WordEntity
  namespaceId: number

  loading: boolean
  fileName: string
  fileNameUri: string

  isValidFile = false
  isFile = true

  @ViewChild('fileInput') fileInput: ElementRef

  userPreference: Observable<PreferenceEntity> = null

  constructor(
    private eventManager: EventManager,
    private fileService: FileService,
    private preferenceService: PreferenceService
  ) {
    this.userPreference = this.preferenceService.getUserPreferences()
    this.eventManager.subscribe('dropbox.file.saved', _ => {
      this.file = null
      this.loading = false
      this.fileName = null
      this.fileNameUri = null
      this.isValidFile = false
    })
  }

  ngOnInit() {
  }

  onFileChange(event) {
    this.userPreference.subscribe(pref => {
      if (event.target.files.length > 0) {
        this.fileName = event.target.files[0].name
        this.file = event.target.files[0]
        this.isValidFile = true
        this.namespaceId = pref.namespace.id
        // clear state
        event.target.value = ''
      }
    })
  }

  onFileUriChange() {
    this.userPreference.subscribe(pref => {
      this.namespaceId = pref.namespace.id
      this.isValidFile = this.fileNameUri.startsWith('http://') || this.fileNameUri.startsWith('https://')
    })
  }

  toggleSource() {
    this.isFile = !this.isFile
    this.fileName = null
    this.fileNameUri = null
    this.isValidFile = false
    this.loading = false
  }

  private buildFilePayload(): FormData {
    const req = new FormData()
    req.append('file', this.file)
    req.append('namespaceId', '' + this.namespaceId)
    req.append('wordId', '' + this.wordEntity.id)
    req.append('wordName', '' + this.wordEntity.name)
    return req
  }

  private buildFileUriPayload(): FormData {
    const req = new FormData()
    req.append('fileUri', this.fileNameUri)
    req.append('namespaceId', '' + this.namespaceId)
    req.append('wordId', '' + this.wordEntity.id)
    req.append('wordName', '' + this.wordEntity.name)
    return req
  }

  onSubmit() {
    this.loading = true

    if (this.isFile) {
      const fileFormData = this.buildFilePayload()
      this.fileService.uploadFile(fileFormData)
    } else {
      const fileUriFormData = this.buildFileUriPayload()
      this.fileService.uploadFileUri(fileUriFormData)
    }
  }
}

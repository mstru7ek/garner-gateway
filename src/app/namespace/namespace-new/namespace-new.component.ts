import { Router } from '@angular/router'
import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { NamespaceService } from 'app/service'
import { NamespaceEntity } from 'app/model'
import { LoggerService, Logger, FormUtils } from 'app/shared'

@Component({
  selector: 'grn-namespace-new',
  templateUrl: './namespace-new.component.html',
  styleUrls: ['./namespace-new.component.scss']
})
export class NamespaceNewComponent implements OnInit {

  formErrors: { [key: string]: string } = {
    'name': '',
  }

  validationMessages = {
    'name': {
      'required': 'Name is required.',
      'minlength': 'Name cannot be less than 3 characters long.',
      'maxlength': 'Name cannot be more than 50 characters long.',
    },
  }
  private logger: Logger

  namespaceForm: FormGroup

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private namespaceService: NamespaceService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('NamespaceNewComponent')
  }

  ngOnInit() {
    this.buildForm()
  }

  onSubmit() {
    const newNamespaceEntity: NamespaceEntity = {
      name: this.namespaceForm.value.name,
    }

    this.namespaceService.create(newNamespaceEntity).subscribe( namespace => {
      this.logger.info('+ new namespace, namespace.id', namespace.id)
      this.logger.info('+ new namespace, namespace.name', namespace.name)
      const requestUrlTree = this.router.createUrlTree(['/namespace'])
      this.router.navigateByUrl(requestUrlTree)
    })
  }

  onCancel() {
    const requestUrlTree = this.router.createUrlTree(['/namespace'])
    this.router.navigateByUrl(requestUrlTree)
  }

  buildForm() {
    this.namespaceForm = this.formBuilder.group({
      'name': ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    })
    this.namespaceForm.valueChanges.subscribe(_ => this.onValueChanged())
    this.onValueChanged() // (re)set validation messages now
  }

  onValueChanged() {
    this.formErrors = FormUtils.buildValidationErrors(this.namespaceForm, this.formErrors, this.validationMessages)
  }
}

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgModule, } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'

import { SharedModule } from 'app/shared/shared.module'
import { ServiceModule } from 'app/service/service.module'

import {
  NamespaceViewComponent,
  NamespaceListComponent,
  NamespaceListContextMenuComponent,
  NamespaceNewComponent,
} from './';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    CommonModule,
    SharedModule,
    ServiceModule,
  ],
  declarations: [
    NamespaceViewComponent,
    NamespaceListComponent,
    NamespaceListContextMenuComponent,
    NamespaceNewComponent,
  ],
  entryComponents: [
    NamespaceListContextMenuComponent,
  ],
  exports: [
    NamespaceViewComponent,
    NamespaceListComponent,
    NamespaceNewComponent,
  ]
})
export class NamespaceModule { }

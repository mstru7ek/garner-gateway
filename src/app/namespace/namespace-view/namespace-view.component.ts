import { ActivatedRoute, Router } from '@angular/router'
import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { NamespaceService } from 'app/service'
import { NamespaceEntity } from 'app/model'
import { LoggerService, Logger, FormUtils } from 'app/shared'
import { Observable } from 'rxjs';

@Component({
  selector: 'grn-namespace-view',
  templateUrl: './namespace-view.component.html',
  styleUrls: ['./namespace-view.component.scss']
})
export class NamespaceViewComponent implements OnInit {

  formErrors: { [key: string]: string } = {
    'name': '',
  }

  validationMessages = {
    'name': {
      'required': 'Name is required.',
      'minlength': 'Name cannot be less than 3 characters long.',
      'maxlength': 'Name cannot be more than 50 characters long.',
    },
  }
  private logger: Logger

  editMode = false
  namespaceForm: FormGroup

  namespaceId: number
  namespace: Observable<NamespaceEntity>

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private namespaceService: NamespaceService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('NamespaceViewComponent')
  }

  ngOnInit() {
    this.namespaceId = +this.activatedRoute.snapshot.paramMap.get('id')
    this.namespace = this.namespaceService.get(this.namespaceId)
    this.buildForm()
  }

  onSubmit() {
    this.editMode = false
    const namespaceEntityUpdate: NamespaceEntity = {
      id: this.namespaceId,
      name: this.namespaceForm.value.name,
      active: true,
    }

    this.namespaceService.update(namespaceEntityUpdate).subscribe( _ => {
      this.logger.info('+ namespace update done, namespace.id', namespaceEntityUpdate.id)
      this.logger.info('+ namespace update done, namespace.name', namespaceEntityUpdate.name)
      const requestUrlTree = this.router.createUrlTree(['/namespace'])
      this.router.navigateByUrl(requestUrlTree)
    })
  }

  onEdit() {
    this.editMode = true
    this.buildForm()
  }

  onCancel() {
    this.editMode = false
    this.namespaceForm.reset()

    const requestUrlTree = this.router.createUrlTree(['/namespace'])
    this.router.navigateByUrl(requestUrlTree)
  }

  buildForm() {
    this.namespace.subscribe(namespace => {
      this.namespaceForm = this.formBuilder.group({
        'name': [namespace.name, [ Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      })
      this.namespaceForm.valueChanges.subscribe(_ => this.onValueChanged())
      this.onValueChanged() // (re)set validation messages now
      this.editMode = true
    })
  }

  onValueChanged() {
    this.formErrors = FormUtils.buildValidationErrors(this.namespaceForm, this.formErrors, this.validationMessages)
  }
}

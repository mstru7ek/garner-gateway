import { Component, OnInit, ViewChild, Type } from '@angular/core'
import { Router } from '@angular/router'

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

import { ContextMenuComponent, ContextMenuResult, LoggerService, Logger, PreloadService } from 'app/shared'
import { NamespaceEntity, PreferenceEntity } from 'app/model'
import { NamespaceService, PreferenceService } from 'app/service'
import { Observable } from 'rxjs';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class NamespaceListContextMenuComponent  {

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-namespace-list',
  templateUrl: './namespace-list.component.html',
  styleUrls: ['./namespace-list.component.scss']
})
export class NamespaceListComponent implements OnInit {
  private logger: Logger

  @ViewChild(ContextMenuComponent)
  contextMenu: ContextMenuComponent<NamespaceListContextMenuComponent>
  contextMenuContent: Type<any> = NamespaceListContextMenuComponent

  selectedNamespace: NamespaceEntity
  userNamespaces: Observable<NamespaceEntity[]>|null = null
  userPreference: Observable<PreferenceEntity>|null = null

  constructor(
    private router: Router,
    private namespaceService: NamespaceService,
    private preferenceService: PreferenceService,
    private loggerService: LoggerService,
    private preloadService: PreloadService,
  ) {
    this.logger = this.loggerService.getInstance('NamespaceListComponent')
  }

  ngOnInit() {
    this.preloadService.show()
    this.userPreference = this.preferenceService.getUserPreferences()
    this.userNamespaces = this.namespaceService.list()
    this.userNamespaces.subscribe( _ => this.preloadService.hide())
  }

  openContextMenu( namespace: NamespaceEntity ) {
    this.selectedNamespace = namespace
    this.contextMenu.open()
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('SELECT' === $event.action) {
        this.preloadService.show()
        this.preferenceService.setDefaultNamspace(this.selectedNamespace).subscribe(namespace => {
          this.logger.info('Default namespace changed', this.selectedNamespace)
          this.userPreference = this.preferenceService.getUserPreferences()
          this.userPreference.subscribe( _ => this.preloadService.hide())
        })
      } else if ('EDIT' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/namespace', 'edit', this.selectedNamespace.id])
        this.router.navigateByUrl(requestUrlTree)
      } else {
        this.logger.error('Invalid context menu event action = ' + $event.action)
      }
    }
  }

}

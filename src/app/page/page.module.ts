import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { LoginPageModule } from './login-page/login-page.module'
import { LogoutPageModule } from './logout-page/logout-page.module'
import { RegisterPageModule } from './register-page/register-page.module';
import { HomePageModule } from './home-page/home-page.module'

import { NamespaceModifyPageModule } from './namespace-modify-page/namespace-modify-page.module'
import { NamespaceListPageModule } from './namespace-list-page/namespace-list-page.module'
import { NamespaceNewPageModule } from './namespace-new-page/namespace-new-page.module'
import { PreferencePageModule } from './preference-page/preference-page.module'
import { AccessGrantedPageModule } from './access-granted-page/access-granted-page.module'
import { TopicListPageModule } from './topic-list-page/topic-list-page.module'
import { TopicViewPageModule } from './topic-view-page/topic-view-page.module'
import { WordModifyPageModule } from './word-modify-page/word-modify-page.module'
import { WordListPageModule } from './word-list-page/word-list-page.module'
import { WordViewPageModule } from './word-view-page/word-view-page.module'
import { WordNewPageModule } from './word-new-page/word-new-page.module'
import { MapViewPageModule } from './map-view-page/map-view-page.module'
import { MapTopicViewPageModule } from './map-topic-view-page/map-topic-view-page.module'
import { GraphViewPageModule } from './graph-view-page/graph-view-page.module';
import { LookBackPageModule } from './look-back-page/look-back-page.module';
import { SyncPageModule } from './sync-page/sync-page.module';

@NgModule({
  imports: [
    CommonModule,
    LoginPageModule,
    LogoutPageModule,
    RegisterPageModule,
    HomePageModule,
    WordViewPageModule,
    NamespaceModifyPageModule,
    NamespaceListPageModule,
    NamespaceNewPageModule,
    PreferencePageModule,
    AccessGrantedPageModule,
    TopicListPageModule,
    TopicViewPageModule,
    WordModifyPageModule,
    WordListPageModule,
    LookBackPageModule,
    WordNewPageModule,
    MapViewPageModule,
    MapTopicViewPageModule,
    GraphViewPageModule,
    SyncPageModule,
  ],
  declarations: []
})
export class PageModule { }

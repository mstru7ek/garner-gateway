import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TopicListPageComponent } from './topic-list-page.component'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { TopicModule } from 'app/topic/topic.module';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const TOPICS_LIST_PAGE_ROUTES: Routes = [{
  path: 'topic',
  component: TopicListPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Topics List'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(TOPICS_LIST_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    TopicModule,

  ],
  declarations: [TopicListPageComponent]
})
export class TopicListPageModule { }

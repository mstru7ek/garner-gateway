import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { NamespaceModule } from 'app/namespace/namespace.module'

import { NamespaceNewPageComponent } from './namespace-new-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const NAMESPACE_NEW_PAGE_ROUTES: Routes = [{
  path: 'namespace/new',
  component: NamespaceNewPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Create Namespace'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(NAMESPACE_NEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    NamespaceModule
  ],
  declarations: [NamespaceNewPageComponent]
})
export class NamespaceNewPageModule { }

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { TopicModule } from 'app/topic/topic.module';

import { TopicViewPageComponent } from './topic-view-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const TOPIC_VIEW_PAGE_ROUTES: Routes = [{
  path: 'topic/:id',
  component: TopicViewPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Topic View'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(TOPIC_VIEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    TopicModule,
  ],
  declarations: [TopicViewPageComponent]
})
export class TopicViewPageModule { }

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DEF_ROUTE_CONFIG } from 'app/shared';
import { UserRouteAccessService } from 'app/shared/auth';
import { LoginPageComponent } from './login-page.component';

export const LOGIN_PAGE_ROUTES: Routes = [{
    path: 'login',
    component: LoginPageComponent,
    canActivate: [ UserRouteAccessService ],
    data: {
        authorities: [ ],
        pageTitle: 'Login'
    },
}]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot( LOGIN_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    ],
    declarations: [ LoginPageComponent ],
})
export class LoginPageModule {}

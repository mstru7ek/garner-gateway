import { AfterViewInit, Component, ElementRef, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/service';
import { PreloadService } from 'app/shared';
import { StateStorageService } from 'app/shared/auth';

@Component({
    selector: 'grn-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements AfterViewInit {
    authenticationError: boolean
    password: string
    rememberMe: boolean
    username: string
    credentials: any

    constructor(
        private router: Router,
        private renderer: Renderer,
        private elementRef: ElementRef,
        private loginService: LoginService,
        private preloadService: PreloadService,
        private stateStorageService: StateStorageService,
    ) {
        this.credentials = {}
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', [])
    }

    cancel() {
        this.credentials = {
            username: null,
            password: null,
            rememberMe: true
        }
        this.authenticationError = false
    }

    login() {
        this.preloadService.show()

        this.loginService.login({
            username: this.username,
            password: this.password,
            rememberMe: this.rememberMe
        }).then(() => {
            this.preloadService.hide()
            this.authenticationError = false
            if (this.router.url === '/register' || (/activate/.test(this.router.url)) ||
                this.router.url === '/finishReset' || this.router.url === '/requestReset') {
                this.router.navigate([''])
            }

            this.loginService.broadcastSuccessEvent()

            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is succesful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl()
            if (redirect) {
                this.router.navigate([redirect])
            } else {
                this.router.navigate([''])
            }
        }).catch(() => {
            this.preloadService.hide()
            this.authenticationError = true
        })
    }
}

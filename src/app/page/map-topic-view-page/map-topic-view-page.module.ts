import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { MapModule } from 'app/map/map.module'

import { MapTopicViewPageComponent } from './map-topic-view-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const MAP_TOPIC_VIEW_PAGE_ROUTES: Routes = [{
  path: 'map/:id',
  component: MapTopicViewPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Word Map'
  },
},
{
  path: 'map/:id/word/:wId',
  component: MapTopicViewPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Word Map'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(MAP_TOPIC_VIEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    MapModule,
  ],
  declarations: [MapTopicViewPageComponent]
})
export class MapTopicViewPageModule { }

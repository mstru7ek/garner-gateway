import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { WordModule } from 'app/word/word.module'

import { WordNewPageComponent } from './word-new-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const WORD_NEW_PAGE_ROUTES: Routes = [{
  path: 'word/new',
  component: WordNewPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [ Authorities.ROLE_USER ],
    pageTitle: 'Create Word'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( WORD_NEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    WordModule,
  ],
  declarations: [ WordNewPageComponent ]
})
export class WordNewPageModule { }

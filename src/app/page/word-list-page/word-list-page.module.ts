import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { WordModule } from 'app/word/word.module'

import { WordListPageComponent } from './word-list-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const WORDS_LIST_PAGE_ROUTES: Routes = [{
  path: 'word',
  component: WordListPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Words List'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(WORDS_LIST_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    WordModule,
  ],
  declarations: [WordListPageComponent]
})
export class WordListPageModule { }

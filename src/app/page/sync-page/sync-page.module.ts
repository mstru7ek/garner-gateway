import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Authorities } from 'app/model';
import { PreferenceModule } from 'app/preference/preference.module';
import { UserRouteAccessService } from 'app/shared/auth';
import { SyncPageComponent as SyncPageComponent } from './sync-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const SYNC_PAGE_ROUTES: Routes = [{
  path: 'sync',
  component: SyncPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Sync'
  }
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(SYNC_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    PreferenceModule
  ],
  declarations: [SyncPageComponent]
})
export class SyncPageModule { }

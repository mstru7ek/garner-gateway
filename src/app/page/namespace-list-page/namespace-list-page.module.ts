import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { NamespaceModule } from 'app/namespace/namespace.module'

import { NamespaceListPageComponent } from './namespace-list-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const NAMESPACE_PAGE_ROUTES: Routes = [{
  path: 'namespace',
  component: NamespaceListPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Namespace'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(NAMESPACE_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    NamespaceModule
  ],
  declarations: [NamespaceListPageComponent],
})
export class NamespaceListPageModule { }

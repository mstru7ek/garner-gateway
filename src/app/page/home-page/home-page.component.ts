import { Component, OnInit } from '@angular/core'
import { Principal } from 'app/shared/auth'
import { AccountInfo } from 'app/model';

@Component({
    selector: 'grn-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: [ 'home-page.css' ]
})
export class HomePageComponent implements OnInit {
    account: AccountInfo

    constructor(
        private principal: Principal
    ) { }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.account = account
        })
    }

    registerAuthenticationSuccess() {
    }

    isAuthenticated() {
        return this.principal.isAuthenticated()
    }
}

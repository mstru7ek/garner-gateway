import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Route, RouterModule } from '@angular/router'
import { HomePageComponent } from './home-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared'
import { UserRouteAccessService } from 'app/shared/auth'
import { SharedModule } from 'app/shared/shared.module';

export const HOME_PAGE_ROUTE: Route = {
    path: '',
    component: HomePageComponent,
    canActivate: [ UserRouteAccessService ],
    data: {
        authorities: [],
        pageTitle: 'Garner'
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot([ HOME_PAGE_ROUTE ], DEF_ROUTE_CONFIG),
        SharedModule,
    ],
    declarations: [
        HomePageComponent,
    ],
})
export class HomePageModule {}

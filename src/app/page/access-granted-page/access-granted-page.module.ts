import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Authorities } from 'app/model';
import { PreferenceModule } from 'app/preference/preference.module';
import { UserRouteAccessService } from 'app/shared/auth';
import { AccessGrantedPageComponent } from './access-granted-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const ACCESS_GRANTED_PAGE_ROUTES: Routes = [
{
    path: 'preferences/access-granted/:state/:code',
    component: AccessGrantedPageComponent,
    canActivate: [UserRouteAccessService],
    data: {
      authorities: [Authorities.ROLE_USER],
      pageTitle: 'Access Granted'
    }
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(ACCESS_GRANTED_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    PreferenceModule
  ],
  declarations: [AccessGrantedPageComponent]
})
export class AccessGrantedPageModule {}

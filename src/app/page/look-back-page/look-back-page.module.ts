import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { WordModule } from 'app/word/word.module'

import { LookBackPageComponent } from './look-back-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const LOOK_BACK_PAGE_ROUTES: Routes = [
  {
    path: 'lookback',
    redirectTo: 'lookback/0',
  },
  {
    path: 'lookback/:pageNum',
    component: LookBackPageComponent,
    canActivate: [UserRouteAccessService],
    data: {
      authorities: [Authorities.ROLE_USER],
      pageTitle: 'Look Back'
    },
  }]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(LOOK_BACK_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    WordModule,
  ],
  declarations: [LookBackPageComponent]
})
export class LookBackPageModule { }

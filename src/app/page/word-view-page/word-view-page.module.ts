import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { WordModule } from 'app/word/word.module'

import { WordViewPageComponent } from './word-view-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const WORD_VIEW_PAGE_ROUTES: Routes = [{
  path: 'word/view/:id',
  component: WordViewPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [ Authorities.ROLE_USER ],
    pageTitle: 'Word View'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( WORD_VIEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    WordModule,
  ],
  declarations: [ WordViewPageComponent ]
})
export class WordViewPageModule { }

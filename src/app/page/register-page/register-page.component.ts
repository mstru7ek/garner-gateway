import { Component, OnInit } from '@angular/core';
import { RegistrationForm } from 'app/model';
import { PreloadService } from 'app/shared';
import { AccountService } from 'app/shared/auth';

@Component({
    selector: 'grn-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

    firstName: string
    lastName: string
    login: string
    password: string
    email: string

    isRegistrationSuccess = false;

    constructor(
        private accountService: AccountService,
        private preloadService: PreloadService,
    ) { }

    ngOnInit() {
    }

    register() {
        this.preloadService.show()

        const form = new RegistrationForm( this.firstName, this.lastName, this.login, this.password, this.email)

        this.accountService.register(form).subscribe(() => {
            this.preloadService.hide()
            this.isRegistrationSuccess = true
        }, () => {
            this.preloadService.hide()
            this.isRegistrationSuccess = false
        })

    }
}

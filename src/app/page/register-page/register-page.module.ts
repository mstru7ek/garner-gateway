import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { RegisterPageComponent } from './register-page.component';
import { UserRouteAccessService } from 'app/shared/auth'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const REGISTER_PAGE_ROUTES: Routes = [{
    path: 'register',
    component: RegisterPageComponent,
    canActivate: [UserRouteAccessService],
    data: {
        authorities: [],
        pageTitle: 'Register'
    },
}]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot(REGISTER_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    ],
    declarations: [RegisterPageComponent],
})
export class RegisterPageModule { }

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { GraphModule } from 'app/graph/graph.module'

import { GraphViewPageComponent } from './graph-view-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const GRAPH_VIEW_PAGE_ROUTES: Routes = [{
  path: 'graph/:id',
  component: GraphViewPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Graph'
  },
}, {
  path: 'graph/:id/word/:wId',
  component: GraphViewPageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Graph'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(GRAPH_VIEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    GraphModule,
  ],
  declarations: [GraphViewPageComponent]
})
export class GraphViewPageModule { }

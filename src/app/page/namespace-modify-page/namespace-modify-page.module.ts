import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { Routes, RouterModule } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { NamespaceModule } from 'app/namespace/namespace.module'

import { NamespaceModifyPageComponent } from './namespace-modify-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const NAMESPACE_MODIFY_PAGE_ROUTES: Routes = [{
  path: 'namespace/edit/:id',
  component: NamespaceModifyPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
      authorities: [ Authorities.ROLE_USER ],
      pageTitle: 'Namespace Edit'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( NAMESPACE_MODIFY_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    NamespaceModule,
  ],
  declarations: [ NamespaceModifyPageComponent ]
})
export class NamespaceModifyPageModule { }

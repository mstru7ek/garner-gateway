import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'
import { MapModule } from 'app/map/map.module'

import { MapViewPageComponent } from './map-view-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const MAP_VIEW_PAGE_ROUTES: Routes = [{
  path: 'map',
  component: MapViewPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Word Map'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(MAP_VIEW_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    MapModule,
  ],
  declarations: [MapViewPageComponent]
})
export class MapViewPageModule { }

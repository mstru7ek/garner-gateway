import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { UserRouteAccessService } from 'app/shared/auth'
import { Authorities } from 'app/model'

import { WordModifyPageComponent } from './word-modify-page.component'
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const WORD_MODIFY_PAGE_ROUTES: Routes = [{
  path: 'word/modify/:id',
  component: WordModifyPageComponent,
  canActivate: [ UserRouteAccessService ],
  data: {
    authorities: [ Authorities.ROLE_USER ],
    pageTitle: 'Word Modify'
  },
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot( WORD_MODIFY_PAGE_ROUTES, DEF_ROUTE_CONFIG),
  ],
  declarations: [ WordModifyPageComponent ]
})
export class WordModifyPageModule { }

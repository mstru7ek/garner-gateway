import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventManager, LoginService } from 'app/service';
import { PreloadService } from 'app/shared';

@Component({
  selector: 'grn-logout-page',
  templateUrl: './logout-page.component.html',
  styles: ['./logout-page.component.scss']
})
export class LogoutPageComponent implements OnInit {

  constructor(
    private router: Router,
    private loginService: LoginService,
    private preloadService: PreloadService,
    private eventManager: EventManager
  ) {
    this.eventManager.subscribe('logoutSuccess', _ => {
      this.preloadService.hide()
      this.router.navigate([''])
    })
  }

  ngOnInit() {
    this.preloadService.show()
    this.loginService.logout()
  }

}

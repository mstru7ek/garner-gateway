import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { DEF_ROUTE_CONFIG } from 'app/shared';
import { UserRouteAccessService } from 'app/shared/auth';
import { SharedModule } from 'app/shared/shared.module';
import { LogoutPageComponent } from './logout-page.component';

export const LOGOUT_PAGE_ROUTE: Route = {
    path: 'logout',
    component: LogoutPageComponent,
    canActivate: [ UserRouteAccessService ],
    data: {
        authorities: [],
        pageTitle: 'Logout'
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot([ LOGOUT_PAGE_ROUTE ], DEF_ROUTE_CONFIG),
        SharedModule,
    ],
    declarations: [
        LogoutPageComponent,
    ],
})
export class LogoutPageModule { }

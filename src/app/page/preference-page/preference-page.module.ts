import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Authorities } from 'app/model';
import { PreferenceModule } from 'app/preference/preference.module';
import { UserRouteAccessService } from 'app/shared/auth';
import { PreferencePageComponent } from './preference-page.component';
import { DEF_ROUTE_CONFIG } from 'app/shared';

export const PREFERENCE_PAGE_ROUTES: Routes = [{
  path: 'preferences',
  component: PreferencePageComponent,
  canActivate: [UserRouteAccessService],
  data: {
    authorities: [Authorities.ROLE_USER],
    pageTitle: 'Preferences'
  }
}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(PREFERENCE_PAGE_ROUTES, DEF_ROUTE_CONFIG),
    PreferenceModule
  ],
  declarations: [PreferencePageComponent]
})
export class PreferencePageModule { }

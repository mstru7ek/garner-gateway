import { Router } from '@angular/router'
import { Component, OnInit, ViewChild, Type } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

import { BehaviorSubject, Observable } from 'rxjs'

import { ContextMenuComponent, ContextMenuResult, LoggerService, Logger } from 'app/shared'
import { WordEntity, Page } from 'app/model'
import { WordService } from 'app/service'
import { combineLatest, flatMap, debounceTime, tap } from 'rxjs/operators';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class WordListContextMenuComponent {

  selectedWord: WordEntity

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-word-list',
  templateUrl: './word-list.component.html',
  styleUrls: ['./word-list.component.scss'],
  entryComponents: [
    WordListContextMenuComponent
  ]
})
export class WordListComponent implements OnInit {

  logger: Logger

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<WordListContextMenuComponent>
  contextMenuContent: Type<any> = WordListContextMenuComponent

  pageable: Observable<Page<WordEntity>>

  pageNumber = new BehaviorSubject<number>(0)
  searchText = new BehaviorSubject('')
  clear = new BehaviorSubject(false)

  selectedWord: WordEntity
  namespaceWordList: Observable<WordEntity[]>

  constructor(
    private router: Router,
    private wordService: WordService,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('WordListComponent')
    this.pageable = this.pageNumber.pipe(
      combineLatest(
        this.searchText.pipe(
          debounceTime(800),
          tap(_ => { this.clear.next(true) })
        ),
        (pageNo, search) => ({ search, pageNo })
      ),
      flatMap(req => this.wordService.list(req.search, req.pageNo))
    )
  }

  ngOnInit() {
  }

  onSerchText(searchTerm: string) {
    this.searchText.next(searchTerm)
  }

  openContextMenu(word: WordEntity) {
    this.selectedWord = word
    this.contextMenu.open()
    this.contextMenu.componentInstance().selectedWord = word
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
        return
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.selectedWord.topic.id, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('MAP' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/map', this.selectedWord.topic.id, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      } else {
        this.logger.error('Invalid context menu event action = ' + $event.action)
      }
    }
  }
}

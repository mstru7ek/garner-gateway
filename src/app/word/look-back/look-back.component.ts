import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FileMetadata, Page } from 'app/model';
import { FileService } from 'app/service';
import { Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

// Sequence generator
const range = (start, stop, step) =>
  Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + (i * step));

// Aliases
const max = Math.max;
const min = Math.min;

// Pagination helper representations
export interface HasNext {
  has: boolean,
  id: number
};

export interface HasPrevious {
  has: boolean,
  id: number
};

interface PageRange {
  begin: number,
  end: number
};

// Paginations consts
const PAGINATION_SIZE = 7;
const PD = PAGINATION_SIZE - 1;
const LR = (PAGINATION_SIZE - 1) / 2;

@Component({
  selector: 'grn-look-back',
  templateUrl: './look-back.component.html',
  styleUrls: ['./look-back.component.scss'],
})
export class LookBackComponent {

  pageable$: Observable<Page<FileMetadata>>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fileService: FileService
  ) {

    this.pageable$ = this.activatedRoute.paramMap
      .pipe(
        flatMap(param =>
          this.fileService.lookBack(+param.get('pageNum'))
        )
      );
  }

  onSelectedItem(file: FileMetadata) {
    const requestUrlTree = this.router.createUrlTree(['/word', 'view', file.wordId])
    this.router.navigateByUrl(requestUrlTree)
  }

  paginationRange(page: Page<FileMetadata>): PageRange {
    const _all_pages_ = page.totalPages - 1;
    const _begin_it = max(page.number - LR, 0);
    const _end_it = min(page.number + LR, _all_pages_);

    const begin = max(0, _begin_it - (PD - (_end_it - _begin_it)));

    const end = min(_all_pages_, _end_it + (PD - (_end_it - _begin_it)));

    return { begin: begin, end: end};
  }

  buildPagination(page: Page<FileMetadata>) {
    const pagination = this.paginationRange(page);
    return range(pagination.begin, pagination.end, 1);
  }

  hasPrevious(page: Page<FileMetadata>): HasPrevious {
    const pagination = this.paginationRange(page);
    return {
      has: pagination.begin > 0,
      id: max(pagination.begin - LR, 0)
    };
  }

  hasNext(page: Page<FileMetadata>): HasNext {
    const pagination = this.paginationRange(page);
    return {
      has: pagination.end < page.totalPages - 1,
      id: min(pagination.end + LR, page.totalPages - 1)
    };
  }

};

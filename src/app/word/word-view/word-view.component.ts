
import { Component, OnInit, Type, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AudioFileEntity, FileMetadata, SiblingEntity, SuggestWordEntity, WordCoordinateEntity, WordEntity } from 'app/model';
import { AudioFileService, EventManager, FileService, SiblingService, WordService } from 'app/service';
import { ContextMenuComponent, ContextMenuResult, FormUtils } from 'app/shared';
import { PreloadService } from 'app/shared/preload.service';
import { Observable, of as observableOf } from 'rxjs';
import { share } from 'rxjs/operators';

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class WordViewContextMenuComponent {

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  templateUrl: './action-context-menu.content.html',
  styleUrls: ['./action-context-menu.content.scss'],
})
export class ActionWordViewContextMenuComponent {

  wordEntity: WordEntity

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-word-view',
  templateUrl: './word-view.component.html',
  styleUrls: ['./word-view.component.scss'],
  entryComponents: [
    WordViewContextMenuComponent
  ]
})
export class WordViewComponent implements OnInit {

  formErrors: { [key: string]: string } = {
    'name': '',
    'translation': '',
    'sentence': '',
    'transcription': ''
  }

  private validationMessages = {
    'name': {
      'required': 'Name is required.',
      'minlength': 'Name cannot be less than 3 characters long.',
      'maxlength': 'Name cannot be more than 50 characters long.',
    },
    'translation': {
      'required': 'Translation is required.',
      'maxlength': 'Translation cannot be more than 50 characters long.'
    },
    'sentence': {
      'maxlength': 'Sentence cannot be more than 500 characters long.'
    },
    'transcription': {
      'maxlength': 'Transcription cannot be more than 50 characters long.'
    },
  }

  @ViewChild(ContextMenuComponent)
  contextMenu: ContextMenuComponent<WordViewContextMenuComponent>
  contextMenuContent: Type<any> = WordViewContextMenuComponent

  @ViewChild('action')
  actionContextMenu: ContextMenuComponent<ActionWordViewContextMenuComponent>
  actionContextMenuContent: Type<any> = ActionWordViewContextMenuComponent

  editMode = false
  mapWidgetVisible = true

  wordEntity: Observable<WordEntity> = null

  private wordId: number = null
  private wordEntityRef: WordEntity = null

  wordForm: FormGroup
  wordCoordinate: WordCoordinateEntity = null

  siblings: Observable<SiblingEntity[]> = null
  private selectedSibling: SiblingEntity = null
  audioFiles$: Observable<AudioFileEntity[]> = null
  storageFiles$: Observable<FileMetadata[]> = null

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private wordService: WordService,
    private siblingService: SiblingService,
    private audioFileService: AudioFileService,
    private fileService: FileService,
    private eventManager: EventManager,
    private preloadService: PreloadService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(param => {

      this.preloadService.show()

      this.wordId = +param.get('id')
      this.wordEntity = this.wordService.get(this.wordId)

      this.wordEntity.subscribe(wordEntity => {
        this.wordEntityRef = wordEntity
        this.wordCoordinate = {
          latitude: wordEntity.latitude,
          longitude: wordEntity.longitude,
        }
        this.siblings = observableOf(wordEntity.topic.siblings)
        this.audioFiles$ = this.audioFileService.list(this.wordId)
        this.storageFiles$ = this.fileService.findAll(this.wordId)

        this.preloadService.hide()
      })

      this.eventManager.subscribe('dropbox.file.saved', _ => {
        this.storageFiles$ = this.fileService.findAll(this.wordId)
      })

      this.buildForm()

    })
  }

  positionChanged(position: number[]) {
    this.mapWidgetVisible = false
    this.preloadService.show()

    const updateCoordinate: WordCoordinateEntity = {
      latitude: position[0],
      longitude: position[1],
    }
    this.wordService.updateCoordinate(this.wordId, updateCoordinate).subscribe(coordinates => {
      this.wordCoordinate = coordinates
      this.mapWidgetVisible = true
      this.preloadService.hide()
    })
  }

  openContextMenu(sibling: SiblingEntity) {
    this.selectedSibling = sibling
    this.contextMenu.open()
  }

  createSibling(suggestWordEntity: SuggestWordEntity): void {
    this.siblings = null
    this.preloadService.show()

    this.siblingService.create(this.wordId, suggestWordEntity.id).subscribe(() => {
      this.siblings = this.siblingService.list(this.wordId).pipe(share())
      this.siblings.subscribe(_ => {
        this.preloadService.hide()
      })
    })
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedSibling.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('DELETE' === $event.action) {
        this.siblings = null
        const selectedSibling = this.selectedSibling
        this.preloadService.show()
        this.siblingService.delete(this.wordId, this.selectedSibling.id).subscribe(() => {
          this.siblings = this.siblingService.list(this.wordId)
          this.siblings.subscribe(_ => this.preloadService.hide())
        })
      }
    }
    this.selectedSibling = null
  }

  onSubmit() {
    this.editMode = false
    this.preloadService.show()

    const wordEntityOrigin = this.wordEntity
    this.wordEntity = null // hide details
    wordEntityOrigin.subscribe(wordEntity => {

      wordEntity.name = this.wordForm.value.name
      wordEntity.translation = this.wordForm.value.translation
      wordEntity.sentence = this.wordForm.value.sentence
      wordEntity.transcription = this.wordForm.value.transcription

      this.wordService.update(wordEntity).subscribe(() => {
        this.wordEntity = this.wordService.get(this.wordId)
        this.wordEntity.subscribe(_ => this.preloadService.hide())
      })
    })
  }

  onEdit() {
    this.editMode = true
    this.buildForm()
  }

  onCancel() {
    this.editMode = false
    this.wordForm.reset()
  }

  buildForm() {
    this.wordEntity.subscribe(wordEntity => {
      this.wordForm = this.formBuilder.group({
        'name': [wordEntity.name, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
        'translation': [wordEntity.translation, [Validators.required, Validators.maxLength(50)]],
        'sentence': [wordEntity.sentence, [Validators.maxLength(500)]],
        'transcription': [wordEntity.transcription, [Validators.maxLength(50)]]
      })
      this.wordForm.valueChanges.subscribe(_ => this.onValueChanged())
      this.onValueChanged() // (re)set validation messages now
      this.preloadService.hide()
    })
  }

  showActionsContextMenu() {
    this.wordEntity.subscribe(wordEntity => {
      this.actionContextMenu.open()
      this.actionContextMenu.componentInstance().wordEntity = wordEntity
    })
  }

  selectedActionItem($event) {
    if ($event.selected) {
      if ('SYNC' === $event.action) {
        this.audioFiles$ = null
        this.preloadService.show()
        this.audioFiles$ = this.audioFileService.reload(this.wordId)
        this.audioFiles$.subscribe(_ => this.preloadService.hide())
      } else if ('DELETE' === $event.action) {
        this.preloadService.show()
        this.wordService.delete(this.wordId).subscribe(_ => {
          this.preloadService.hide()
          const requestUrlTree = this.router.createUrlTree(['/word'])
          this.router.navigateByUrl(requestUrlTree)
        })
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.wordEntityRef.topic.id, 'word', this.wordEntityRef.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('MAP' === $event.action) {
        this.wordEntity.subscribe(wordEntity => {
          const requestUrlTree = this.router.createUrlTree(['/map', wordEntity.topic.id, 'word', wordEntity.id])
          this.router.navigateByUrl(requestUrlTree)
        })
      }
    }
  }

  onValueChanged() {
    this.formErrors = FormUtils.buildValidationErrors(this.wordForm, this.formErrors, this.validationMessages)
  }
}

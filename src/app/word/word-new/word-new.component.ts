import { Router } from '@angular/router'
import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { LoggerService, Logger, FormUtils, PreloadService } from 'app/shared'
import { WordService } from 'app/service';
import { CreateWordEntity } from 'app/model';

@Component({
  selector: 'grn-word-new',
  templateUrl: './word-new.component.html',
  styleUrls: ['./word-new.component.scss']
})
export class WordNewComponent implements OnInit {

  formErrors: { [key: string]: string } = {
    'name': '',
    'translation': '',
  }

  validationMessages = {
    'name': {
      'required': 'Name is required.',
      'minlength': 'Name cannot be less than 3 characters long.',
      'maxlength': 'Name cannot be more than 50 characters long.',
    },
    'translation': {
      'required': 'Translation is required.',
      'maxlength': 'Translation cannot be more than 50 characters long.'
    },
  }

  private logger: Logger

  createWordEntity: CreateWordEntity = {
      name: '',
      translation: '',
      latitude: 0.0,
      longitude: 0.0,
      sentence: '',
      transcription: ''
  }

  wordForm: FormGroup

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private wordService: WordService,
    private loggerService: LoggerService,
    private preloadService: PreloadService,
  ) {
    this.logger = this.loggerService.getInstance('WordNewComponent')
  }

  ngOnInit() {
    this.buildForm()
  }

  buildForm() {
    this.wordForm = this.formBuilder.group({
      'name': ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      'translation': ['', [Validators.required, Validators.maxLength(50)]],
    })
    this.wordForm.valueChanges.subscribe(_ => this.onValueChanged())
    this.onValueChanged() // (re)set validation messages now
  }

  positionChanged(position: number[]) {
    this.createWordEntity.latitude = position[0]
    this.createWordEntity.longitude = position[1]
    this.logger.info('position changed, createWordEntity', this.createWordEntity )
  }

  onSubmit() {
    this.preloadService.show()
    this.createWordEntity.name = this.wordForm.value.name
    this.createWordEntity.translation = this.wordForm.value.translation
    this.wordService.create(this.createWordEntity).subscribe(wordEntity => {
      this.logger.info('+ new word saved, wordEntity.id', wordEntity.id )
      this.logger.info('+ new word saved, wordEntity', wordEntity )

      this.preloadService.hide()
      const requestUrlTree = this.router.createUrlTree(['/word', 'view', wordEntity.id])
      this.router.navigateByUrl(requestUrlTree)
    })
  }

  onCancel() {
    const requestUrlTree = this.router.createUrlTree(['/word'])
    this.router.navigateByUrl(requestUrlTree)
  }

  onValueChanged() {
    this.formErrors = FormUtils.buildValidationErrors(this.wordForm, this.formErrors, this.validationMessages)
  }

}

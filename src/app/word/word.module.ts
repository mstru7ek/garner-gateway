import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ServiceModule } from 'app/service/service.module';
import { SharedModule } from 'app/shared/shared.module';
import {
  WordListComponent,
  WordListContextMenuComponent,
  ActionWordViewContextMenuComponent,
  LookBackComponent,
  WordNewComponent,
  WordViewComponent,
  WordViewContextMenuComponent } from './';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModalModule,
        CommonModule,
        SharedModule,
        ServiceModule,
    ],
    declarations: [
        LookBackComponent,
        WordListComponent,
        WordListContextMenuComponent,
        WordViewComponent,
        WordViewContextMenuComponent,
        ActionWordViewContextMenuComponent,
        WordNewComponent,
    ],
    entryComponents: [
        WordListContextMenuComponent,
        WordViewContextMenuComponent,
        ActionWordViewContextMenuComponent,
    ],
    exports: [
        WordListComponent,
        LookBackComponent,
        WordViewComponent,
        WordNewComponent,
    ]
})
export class WordModule {}

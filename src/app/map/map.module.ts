import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap'

import { SharedModule } from 'app/shared/shared.module'
import { ServiceModule } from 'app/service/service.module';

import { NamespaceMapComponent, TopicMapComponent, TopicMapContextMenuComponent, NamespaceMapContextMenuComponent } from './'

@NgModule({
  imports: [
    NgbModalModule,
    CommonModule,
    SharedModule,
    ServiceModule,
  ],
  entryComponents: [
    NamespaceMapContextMenuComponent,
    TopicMapContextMenuComponent,
  ],
  declarations: [
    NamespaceMapComponent,
    NamespaceMapContextMenuComponent,
    TopicMapComponent,
    TopicMapContextMenuComponent,
  ],
  exports: [
    NamespaceMapComponent,
    TopicMapComponent
  ]
})
export class MapModule { }

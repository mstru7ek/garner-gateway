import { Component, ElementRef, Input, NgZone, OnInit, Type, ViewChild } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { WordEntity } from 'app/model/'
import { TopicService } from 'app/service'
import { ContextMenuComponent, ContextMenuResult, PreloadService } from 'app/shared'
import { MAP_TILES, defaultIcon } from 'app/shared'
import * as L from 'leaflet'
import 'leaflet.markercluster'

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class TopicMapContextMenuComponent {

  @Input()
  word: WordEntity

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-topic-map',
  templateUrl: './topic-map.component.html',
  styleUrls: ['./topic-map.component.scss']
})
export class TopicMapComponent implements OnInit {

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<TopicMapContextMenuComponent>

  contextMenuContent: Type<any> = TopicMapContextMenuComponent

  @ViewChild('map') private _mapRef: ElementRef

  private map: L.Map = null

  private selectedWord: WordEntity = null
  private topicId: number = null
  private wordId: number = null

  constructor(
    private _zone: NgZone,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private topicService: TopicService,
    private preloadService: PreloadService,
  ) {
  }
  ngOnInit() {

    this.map = L.map(this._mapRef.nativeElement, {
      center: [52.212, 21.020],
      zoom: 15
    })

    const tiles = MAP_TILES.build().addTo(this.map);

    tiles.on('load', _ => {
      this._zone.run(() => {
        this.preloadService.hide()
      })
    })

    this.activatedRoute.paramMap.subscribe(param => {
      this.preloadService.show()

      this.topicId = +param.get('id')
      this.wordId = +param.get('wId')

      this.topicService.get(this.topicId).subscribe(topicWords => {

        const markers = L.markerClusterGroup({
          showCoverageOnHover: false
        })

        for (const word of topicWords) {
          if (word.latitude == null || word.longitude == null) {
            continue
          }

          const marker = L.marker([word.latitude, word.longitude], { icon: defaultIcon })

          if (this.wordId && this.wordId === word.id) {
            // marker.setIcon('/assets/images/marker_greenLocation.png')
          }

          marker.on('click', _ => {
            this._zone.run(() => {
              this.selectedWord = word
              this.contextMenu.open()
              this.contextMenu.componentInstance().word = word
            })
          })
          markers.addLayer(marker)
        }

        this.map.addLayer(markers)
        this.map.fitBounds(markers.getBounds())
      })
    })
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.topicId, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      }
    }
  }

}

import { Component, ElementRef, Input, NgZone, OnInit, Type, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { WordEntity } from 'app/model/';
import { WordService } from 'app/service';
import { ContextMenuComponent, ContextMenuResult, Logger, LoggerService, PreloadService } from 'app/shared';
import { MAP_TILES, defaultIcon } from 'app/shared'
import * as L from 'leaflet'
import 'leaflet.markercluster'

@Component({
  templateUrl: './context-menu.content.html',
  styleUrls: ['./context-menu.content.scss'],
})
export class NamespaceMapContextMenuComponent {

  @Input()
  word: WordEntity

  constructor(
    public activeModal: NgbActiveModal
  ) { }
}

@Component({
  selector: 'grn-namespace-map',
  templateUrl: './namespace-map.component.html',
  styleUrls: ['./namespace-map.component.scss']
})
export class NamespaceMapComponent implements OnInit {

  @ViewChild(ContextMenuComponent)
  private contextMenu: ContextMenuComponent<NamespaceMapContextMenuComponent>
  contextMenuContent: Type<any> = NamespaceMapContextMenuComponent

  @ViewChild('map') private _mapRef: ElementRef

  public selectedWord: WordEntity = null

  constructor(
    private _zone: NgZone,
    private router: Router,
    private wordService: WordService,
    private preloadService: PreloadService
  ) {
  }

  ngOnInit() {

    this.preloadService.show()

    this.wordService.listAll().subscribe(page => {

      const map = L.map(this._mapRef.nativeElement).setView([52.212, 21.020], 13)

      const tiles = MAP_TILES.build().addTo(map);

      tiles.on('load', _ => {
        this._zone.run(() => {
          this.preloadService.hide()
        })
      })

      const markers = L.markerClusterGroup({
        showCoverageOnHover: false
      })

      for (const word of page.content) {
        if (word.latitude == null || word.longitude == null) {
          continue
        }

        const marker = L.marker([word.latitude, word.longitude], { icon: defaultIcon })
        marker.on('click', _ => {
          this._zone.run(() => {
            this.selectedWord = word
            this.contextMenu.open()
            this.contextMenu.componentInstance().word = word
          })
        })
        markers.addLayer(marker)
      }

      map.addLayer(markers)
      map.fitBounds(markers.getBounds())
    })
  }

  selectedItem($event: ContextMenuResult) {
    if ($event.selected) {
      if ('VIEW' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/word', 'view', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
        return
      } else if ('GRAPH' === $event.action) {
        const requestUrlTree = this.router.createUrlTree(['/graph', this.selectedWord.topic.id, 'word', this.selectedWord.id])
        this.router.navigateByUrl(requestUrlTree)
      }
    }
  }

}

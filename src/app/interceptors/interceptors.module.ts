import { NgModule } from '@angular/core'
import { HTTP_INTERCEPTORS } from '@angular/common/http'

import { Ng2Webstorage } from 'ngx-webstorage';

import { AuthInterceptor } from './auth-interceptor'
import { ErrorHandlerInterceptor } from './error-handler-interceptor';

@NgModule({
  imports: [
    Ng2Webstorage
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    }
  ]
})
export class InterceptorsModule { }

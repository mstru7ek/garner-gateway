import { Injectable } from '@angular/core'
import { HttpHandler, HttpRequest, HttpInterceptor, HttpEvent } from '@angular/common/http'
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage'
import { Observable } from 'rxjs'

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private localStorage: LocalStorageService,
        private sessionStorage: SessionStorageService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken')
        if (!!token) {
            const authReq = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) })
            return next.handle(authReq)
        }
        return  next.handle(req)
    }
}


import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EventManager, HttpErrorServiceEvent } from 'app/service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(
    private eventManager: EventManager,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let response = next.handle(req)
    response = response.pipe(catchError((err: HttpErrorResponse) => {

      const httpErrorServiceEvent: HttpErrorServiceEvent = {
        name: 'http.service.error',
        errorResponse: err,
      }
      this.eventManager.broadcast(httpErrorServiceEvent)
      return observableThrowError(err)
    }))
    return response
  }
}

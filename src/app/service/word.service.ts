import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateWordEntity, Page, PreferenceEntity, UpdateWordEntity, WordCoordinateEntity, WordEntity } from 'app/model';
import { EventManager } from 'app/service/event-manager.service';
import { PreferenceService } from 'app/service/preference.service';
import { Observable } from 'rxjs';
import { mergeMap, share } from 'rxjs/operators';

@Injectable()
export class WordService {

  userPreference: Observable<PreferenceEntity> = null
  namespaceWordList: Observable<WordEntity[]> = null

  constructor(
    private httpClient: HttpClient,
    private preferenceService: PreferenceService,
    private eventManager: EventManager,
  ) {
    this.userPreference = this.preferenceService.getUserPreferences()
    this.eventManager.subscribe('preference.namespace.change', _ => {
      this.namespaceWordList = null
      this.userPreference = this.preferenceService.getUserPreferences()
    })
  }

  create(createWordEntity: CreateWordEntity): Observable<WordEntity> {
    this.namespaceWordList = null // clear namespace word list
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.post<WordEntity>('api/namespace/' + namespaceId + '/word', createWordEntity)
    }))
  }

  get(id: number): Observable<WordEntity> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.get<WordEntity>('api/namespace/' + namespaceId + '/word/' + id)
    }), share())
  }

  update(wordEntity: WordEntity): Observable<void> {
    this.namespaceWordList = null // clear namespace word list
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      const updateWordEntity: UpdateWordEntity = {
        name: wordEntity.name,
        translation: wordEntity.translation,
        sentence: wordEntity.sentence,
        transcription: wordEntity.transcription,
      }
      return this.httpClient.put<void>('api/namespace/' + namespaceId + '/word/' + wordEntity.id, updateWordEntity)
    }))
  }

  updateCoordinate(wordId: number, updateCoordinate: WordCoordinateEntity): Observable<WordCoordinateEntity> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.put<WordCoordinateEntity>('api/namespace/' + namespaceId + '/word/' + wordId + '/coordinate', updateCoordinate)
    }))
  }

  delete(wordId: number): Observable<void> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      this.namespaceWordList = null
      const namespaceId = userPreference.namespace.id
      return this.httpClient.delete<void>('api/namespace/' + namespaceId + '/word/' + wordId)
    }))
  }

  list(searchText: string, pageNo: number): Observable<Page<WordEntity>> {
    return this.userPreference.pipe(mergeMap(pref => {
      const namespaceId = pref.namespace.id
      return this.httpClient.get<Page<WordEntity>>(`api/namespace/` + namespaceId + `/word`, {
        params: {
          searchText: searchText,
          page: '' + pageNo,
          sort: 'createdDate,desc'
        }
      })
    }))
  }

  listAll(): Observable<Page<WordEntity>> {
    return this.list('', 0)
  }

}

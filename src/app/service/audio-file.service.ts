import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AudioFileEntity, Page, PreferenceEntity } from 'app/model';
import { EventManager } from 'app/service/event-manager.service';
import { PreferenceService } from 'app/service/preference.service';
import { Observable } from 'rxjs';
import { map, mergeMap, share } from 'rxjs/operators';

@Injectable()
export class AudioFileService {

  userPreference: Observable<PreferenceEntity> = null
  namespaceWordList: Observable<AudioFileEntity[]> = null

  constructor(
    private httpClient: HttpClient,
    private preferenceService: PreferenceService,
    private eventManager: EventManager,
  ) {
    this.userPreference = this.preferenceService.getUserPreferences()
    this.eventManager.subscribe('preference.namespace.change', _ => {
      this.userPreference = this.preferenceService.getUserPreferences()
    })
  }

  list(wordId: number): Observable<AudioFileEntity[]> {
    return this.userPreference.pipe(
      mergeMap(userPreference => {
        const namespaceId = userPreference.namespace.id
        return this.httpClient.get<Page<AudioFileEntity>>('api/namespace/' + namespaceId + '/word/' + wordId + '/audio')
      }),
      map(page => page.content)
    )
  }

  reload(wordId: number): Observable<AudioFileEntity[]> {
    return this.userPreference.pipe(
      mergeMap(userPreference => {
        const namespaceId = userPreference.namespace.id
        return this.httpClient.put<Page<AudioFileEntity>>('api/namespace/' + namespaceId + '/word/' + wordId + '/audio/reload', {})
      }),
      map(page => page.content),
      share()
    )
  }
}

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { LoggerService, Logger } from 'app/shared/logger.service'
import { DbxRedirectUri, DbxAccessToken, DbxAuthentication } from 'app/model'
import { EventManager } from 'app/service/event-manager.service';
import { Observable, Observer } from 'rxjs';
import { AuthServerProvider } from 'app/shared/auth';

@Injectable()
export class DropboxService {

  private logger: Logger

  accessToken: Observable<DbxAccessToken> = null

  constructor(
    private httpClient: HttpClient,
    private loggerService: LoggerService,
    private eventManager: EventManager,
  ) {
    this.logger = this.loggerService.getInstance('DropboxService')
  }

  getAccessToken(): Observable<DbxAccessToken> {
    if (this.accessToken == null) {
      this.accessToken = this.httpClient.get<DbxAccessToken>(`/api/dbx/accessToken/valid`)
    }
    return this.accessToken;
  }

  connect() {
    this.httpClient.get<DbxRedirectUri>(`/api/dbx/connect`).subscribe(resp => {
      this.logger.info(' + Dbx redirectUri: ', resp.redirectUri)
      if (resp.redirectUri) {
        window.location.href = resp.redirectUri
      }
    })
  }

  disconnect() {
    const disconnect = this.httpClient.delete<void>(`/api/dbx/disconnect`)
    disconnect.subscribe(_ => {
      this.accessToken = null
      this.eventManager.broadcast({ name: 'dropbox.access-token.change' })
    })
  }

  synchronize() {
    return this.httpClient.post<void>(`/api/dbx/storage/reload`, {})
  }

  startMigration() {
    this.logger.info('star synchronization')
    const log = this.logger

    return Observable.create( (observer: Observer<string>) => {
        const  eventSource = new EventSource('/api/dbx/storage/reload', { withCredentials: true })
        eventSource.onopen = function(e) {
            log.info('SSE onopen')
        }

        eventSource.addEventListener('message', function(event: MessageEvent) {
            log.info(event.data)
            observer.next(event.data)
        }, false)

        eventSource.addEventListener('close', function(event: MessageEvent) {
            log.info('Event stream closed !!!')

            observer.next(event.data)
            observer.complete()
            eventSource.close()
        }, false)

        eventSource.onerror = error => {
            log.info('readyState:' + eventSource.readyState)
            // async directive unsubsribe on error
            // observer.error('EventSource error: ' + error)
        }

    })
  }

  setAccessAgranted(state: string, code: string) {
    this.httpClient.get<DbxAuthentication>(`/api/dbx/access-granted`, {
      params: {
        state: state,
        code: code
      }
    }).subscribe(resp => {
      this.logger.info(' + dbx access granted, success: ', resp.success)
      if (resp.success) {
        this.accessToken = null
        this.eventManager.broadcast({ name: 'dropbox.access-token.change' })
      }
    })
  }
}

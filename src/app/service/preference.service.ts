import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NamespaceEntity, PreferenceEntity } from 'app/model';
import { EventManager } from 'app/service/event-manager.service';
import { Observable } from 'rxjs';
import { flatMap, shareReplay } from 'rxjs/operators';

@Injectable()
export class PreferenceService {

  userPreference: Observable<PreferenceEntity> = null

  constructor(
    private httpClient: HttpClient,
    private eventManager: EventManager
  ) {
  }

  getUserPreferences(): Observable<PreferenceEntity> {
    if ( this.userPreference == null)  {
      this.userPreference = this.httpClient.get<PreferenceEntity>(`api/preference`).pipe(shareReplay(1))
    }
    return this.userPreference;
  }

  setDefaultNamspace(namespace: NamespaceEntity): Observable<PreferenceEntity> {
    const updatePreferenceRequest = this.httpClient.put<UpdatePreferences>('api/preference', {
      namespace: namespace
    });

    return updatePreferenceRequest.pipe(flatMap( _ => {
      this.userPreference = this.httpClient.get<PreferenceEntity>(`api/preference`).pipe(shareReplay(1))
      this.userPreference.subscribe( preference => {
        this.eventManager.broadcast({ name: 'preference.namespace.change' })
      })
      return this.userPreference
    }))
  }

}

class UpdatePreferences {
  namespace: NamespaceEntity
}

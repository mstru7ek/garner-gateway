import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PreferenceEntity, SiblingEntity, SuggestWordEntity, WordEntity } from 'app/model';
import { EventManager } from 'app/service/event-manager.service';
import { PreferenceService } from 'app/service/preference.service';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

@Injectable()
export class SiblingService {

  userPreference: Observable<PreferenceEntity> = null
  namespaceWordList: Observable<WordEntity[]> = null

  constructor(
    private httpClient: HttpClient,
    private preferenceService: PreferenceService,
    private eventManager: EventManager,
  ) {
    this.userPreference = this.preferenceService.getUserPreferences()
    this.eventManager.subscribe('preference.namespace.change', _ => {
      this.userPreference = this.preferenceService.getUserPreferences()
    })
  }

  list(wordId: number): Observable<SiblingEntity[]> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.get<SiblingEntity[]>('api/namespace/' + namespaceId + '/word/' + wordId + '/sibling')
    }))
  }

  create(wordId: number, siblingWordId: number): Observable<void> {
    const newSibling = {
      wordId: siblingWordId
    }
    return this.userPreference.pipe(
      mergeMap(userPreference => {
        const namespaceId = userPreference.namespace.id
        return this.httpClient.post<void>('api/namespace/' + namespaceId + '/word/' + wordId + '/sibling', newSibling).pipe(
          map(_ => this.eventManager.broadcast({ name: 'namespace.sibling.modify' }))
        )
      })
    )
  }

  delete(wordId: number, siblingWordId: number): Observable<void> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.delete<void>('api/namespace/' + namespaceId + '/word/' + wordId + '/sibling/' + siblingWordId).pipe(map(_ => {
        this.eventManager.broadcast({ name: 'namespace.sibling.modify' })
      }))
    }))
  }

  suggestWord(wordId: number, searchText: string): Observable<SuggestWordEntity[]> {
    const options = {
      params: new HttpParams().set('searchText', searchText)
    }
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.get<SuggestWordEntity[]>('api/namespace/' + namespaceId + '/word/' + wordId + '/sibling/suggest', options)
    }))
  }
}

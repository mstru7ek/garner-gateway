
import { mergeMap, switchMap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse } from '@angular/common/http'

import { PreferenceEntity, FileMetadata, DeleteFileMetedata } from 'app/model'
import { EventManager } from 'app/service/event-manager.service';
import { Logger, LoggerService } from 'app/shared/logger.service';
import { PreferenceService } from './preference.service';
import { Observable } from 'rxjs';
import { Page } from '../model/page';

@Injectable()
export class FileService {

  private logger: Logger

  userPreference: Observable<PreferenceEntity> = null

  constructor(
    private httpClient: HttpClient,
    private preferenceService: PreferenceService,
    private eventManager: EventManager,
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.getInstance('FileService')
    this.userPreference = this.preferenceService.getUserPreferences()
    this.eventManager.subscribe('preference.namespace.change', _ => {
      this.userPreference = this.preferenceService.getUserPreferences()
    })

  }

  uploadFile(fileForm: FormData) {
    const upload = this.httpClient.post<FileMetadata>('/api/dbx/storage/upload', fileForm)
    upload.subscribe(metadata => {
      this.logger.info('+ file upload result', metadata)
      this.eventManager.broadcast({ name: 'dropbox.file.saved' })
    })
  }

  uploadFileUri(fileForm: FormData) {
    const upload = this.httpClient.post<FileMetadata>('/api/dbx/storage/upload/uri', fileForm)
    upload.subscribe(metadata => {
      this.logger.info('+ file upload result', metadata)
      this.eventManager.broadcast({ name: 'dropbox.file.saved' })
    }, e => {
      this.logger.error('Dropbox upload error', e)
    })
  }

  findAll(wordId: number): Observable<FileMetadata[]> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      const images = this.httpClient.get<FileMetadata[]>(`/api/dbx/storage/files`, {
        params: {
          namespaceId: '' + namespaceId,
          wordId: '' + wordId
        }
      })
      return images
    }))
  }

  lookBack(pageNo: number): Observable<Page<FileMetadata>> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      const images = this.httpClient.get<Page<FileMetadata>>(`/api/dbx/storage/files/lookBack`, {
        params: {
          namespaceId: '' + namespaceId,
          page: '' + pageNo,
          sort: 'createdDate,desc',
          size: '5',
        }
      })
      return images
    }))
  }

  deleteImage(url: string): Observable<void> {
    return this.userPreference.pipe(mergeMap(userPreference => {
      const namespaceId = userPreference.namespace.id
      return this.httpClient.delete<void>(`/api/dbx/storage/files`, {
        params: {
          namespaceId: '' + namespaceId,
          url: url
        }
      })
    }))
  }
}

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http';

import {
    WordService,
    LoginService,
    PreferenceService,
    NamespaceService,
    SiblingService,
    TopicService,
    EventManager,
    DropboxService,
    FileService,
    AudioFileService
} from './'

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        EventManager,
        LoginService,
        WordService,
        AudioFileService,
        TopicService,
        SiblingService,
        PreferenceService,
        DropboxService,
        FileService,
        NamespaceService
    ]
})
export class ServiceModule { }

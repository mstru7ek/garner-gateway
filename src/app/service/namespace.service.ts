
import { of as observableOf, Observable } from 'rxjs';

import { mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { NamespaceEntity } from 'app/model'

@Injectable()
export class NamespaceService {

  userNamespaces: NamespaceEntity[]

  constructor(
    private http: HttpClient,
  ) { }

  create(namespace: NamespaceEntity): Observable<NamespaceEntity> {
    this.userNamespaces = null
    return this.http.post<NamespaceEntity>('api/namespace', namespace)
  }

  update(namespace: NamespaceEntity): Observable<void> {
    this.userNamespaces = null
    return this.http.put<void>('api/namespace/' + namespace.id, namespace)
  }

  get(id: number): Observable<NamespaceEntity> {
    return this.http.get<NamespaceEntity>('api/namespace/' + id)
  }

  list(): Observable<NamespaceEntity[]> {
    if (this.userNamespaces) {
      return observableOf(this.userNamespaces)
    }

    // retrieve the userNamespaces data from the server
    return this.http.get<NamespaceEntity[]>('api/namespace').pipe(
      mergeMap(namespaces => {
        if (namespaces) {
          this.userNamespaces = namespaces
        } else {
          this.userNamespaces = null
        }
        return observableOf(this.userNamespaces)
      }))
  }

}

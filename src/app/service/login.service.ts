import { Injectable } from '@angular/core'
import { Principal, AuthServerProvider } from 'app/shared/auth/'
import { EventManager } from 'app/service/event-manager.service';

@Injectable()
export class LoginService {

    constructor(
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private eventManager: EventManager,
    ) {}

    login(credentials, callback?) {
        const cb = callback || function() {}

        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe(data => {
                this.principal.identity(true).then(account => {
                    console.log(account)
                    resolve(data)
                })
                return cb()
            }, err => {
                this.logout()
                reject(err)
                return cb(err)
            })
        })
    }

    broadcastSuccessEvent() {
        this.eventManager.broadcast({
            name: 'authenticationSuccess',
            content: 'Sending Authentication Success'
        })
    }

    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe)
    }

    logout() {
        this.principal.authenticate(null)
        this.authServerProvider.logout().subscribe()
        this.eventManager.broadcast({
            name: 'logoutSuccess',
            content: 'Sending Logout Success'
        })

      }
}

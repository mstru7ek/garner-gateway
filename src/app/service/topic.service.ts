import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PreferenceEntity, TopicEntity, WordEntity, Page } from 'app/model';
import { EventManager } from 'app/service/event-manager.service';
import { PreferenceService } from 'app/service/preference.service';
import { Observable } from 'rxjs';
import { mergeMap, flatMap } from 'rxjs/operators';

@Injectable()
export class TopicService {

  private userPreference: Observable<PreferenceEntity> = null

  constructor(
    private httpClient: HttpClient,
    private preferenceService: PreferenceService,
    private eventManager: EventManager,
  ) {
    this.userPreference = this.preferenceService.getUserPreferences()

    this.eventManager.subscribe('preference.namespace.change', _ => {
      this.userPreference = this.preferenceService.getUserPreferences()
    })
  }

  get(topicId: number): Observable<WordEntity[]> {
    return this.userPreference
      .pipe(
        flatMap(pref => {
          const namespaceId = pref.namespace.id
          return this.httpClient.get<WordEntity[]>('api/namespace/' + namespaceId + '/topic/' + topicId + '/word')
        })
      )
  }

  list(pageNo: number): Observable<Page<TopicEntity>> {
    return this.userPreference
      .pipe(
        mergeMap(pref => {
          const namespaceId = pref.namespace.id
          return this.httpClient.get<Page<TopicEntity>>('api/namespace/' + namespaceId + '/topic', {
            params: {
              page: '' + pageNo,
              sort: 'createdDate,desc'
            }
          })
        })
      )
  }
}

import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import { filter, share } from 'rxjs/operators';

export interface ServiceEvent {
    name: string
    [key: string]: string
}

export interface HttpErrorServiceEvent {
    name: string
    errorResponse: HttpErrorResponse
}

/**
 * An utility class to manage RX events
 */
@Injectable()
export class EventManager {

    observable: Observable<ServiceEvent|HttpErrorServiceEvent>
    observer: Observer<ServiceEvent|HttpErrorServiceEvent>

    constructor() {
        this.observable = Observable.create((observer: Observer<ServiceEvent|HttpErrorServiceEvent>) => {
            this.observer = observer
        }).pipe(share())
    }

    /**
     * Method to broadcast the event to observer
     */
    broadcast(event: ServiceEvent|HttpErrorServiceEvent) {
        if (this.observer != null) {
            this.observer.next(event)
        }
    }

    /**
     * Method to subscribe to an event with callback
     */
    subscribe(eventName: string, callback) {
        const subscriber: Subscription = this.observable.pipe(filter(event => {
            return event.name === eventName
        })).subscribe(callback)
        return subscriber
    }

    /**
     * Method to unsubscribe the subscription
     */
    destroy(subscriber: Subscription) {
        subscriber.unsubscribe()
    }
}

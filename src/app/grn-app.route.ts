import { Route } from '@angular/router'

import { NavBarComponent } from './shared'

export const navbarRoute: Route = {
    path: '',
    component: NavBarComponent,
    outlet: 'navbar'
}
